#include "PMFProcess.h"
#include <QDir>
#include <QFileInfo>
PMFProcess::PMFProcess(QString inputFile,int maxWindowSize, float slope, float initialDistance,float maxDistance):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/ProgressiveMorphologicalFilterSolEMap.exe")
{

//        setWorkingDir(QFileInfo(mCommandPath).absolutePath());
        setWorkingDir(QFileInfo(inputFile).absolutePath());
        setStartupMessage("Launching progressive morphological filter...");

        QFileInfo inputFileInfo(inputFile);

        QStringList inputs;

        inputs << inputFileInfo.absoluteFilePath()<< QString::number(maxWindowSize) << QString::number(slope) << QString::number(initialDistance) <<QString::number(maxDistance);

        addIntputs(inputs);
}

PMFProcess::~PMFProcess()
{

}

