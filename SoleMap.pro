#-------------------------------------------------
#
# Project created by QtCreator 2015-12-03T09:57:41
#
#-------------------------------------------------

# ensure one "debug_and_release" in CONFIG, for clarity...
debug_and_release {
    CONFIG -= debug_and_release
    CONFIG += debug_and_release
}
# ensure one "debug" or "release" in CONFIG so they can be used as
# conditionals instead of writing "CONFIG(debug, debug|release)"...
CONFIG(debug, debug|release) {
    CONFIG -= debug release
    CONFIG += debug
    }

CONFIG(release, debug|release) {
        CONFIG -= debug release
        CONFIG += release
}

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SoleMap
TEMPLATE = app
#QMAKE_LFLAGS += /MANIFESTUAC:\"level=\'requireAdministrator\' uiAccess=\'false\'\"

SOURCES += main.cpp\
        MainWindowSoleMap.cpp \
    ProcessManager/MultiProcess.cpp \
    ProcessManager/Process.cpp \
    ProcessManager/ProcessConcurrent.cpp \
    ProcessManager/ExternalProcess.cpp \
    pcd2plyProcess.cpp \
    SP_ExtractionProcess.cpp \
    ProgressDialog.cpp \
    PassThroughFilterProcess.cpp \
    GroundSegmentationDialog.cpp \
    PMFProcess.cpp \
    AboutSolEMap.cpp \
    DependencyInfoWidget.cpp \
    ClickableImage.cpp

HEADERS  += MainWindowSoleMap.h \
    ProcessManager/MultiProcess.h \
    ProcessManager/Process.h \
    ProcessManager/ProcessConcurrent.h \
    ProcessManager/ExternalProcess.h \
    ui_MainWindowSoleMap.h \
    pcd2plyProcess.h \
    SP_ExtractionProcess.h \
    ProgressDialog.h \
    ui_ProgressDialog.h \
    ProgressiveMorphologicalFilterProcess.h \
    PassThroughFilterProcess.h \
    GroundSegmentationDialog.h \
    PMFProcess.h \
    AboutSolEMap.h \
    DependencyInfoWidget.h \
    ClickableImage.h

FORMS    += MainWindowSoleMap.ui \
    ProgressDialog.ui \
    GroundSegmentationDialog.ui

INCLUDEPATH += ../../Libs/libCC \
                ../../Libs/libCC/cc64/qCC/db_tree \
                ../../Libs/libCC/cc64/CC/include \
                ../../Libs/libCC/cc64/ccViewer \
                ../../Libs/libCC/cc64/build/ccViewer \
                ../../Libs/libCC/cc64/qCC \
                ../../Libs/libCC/cc64/build/qCC \
                ../../Libs/libCC/cc64/qCC/ui_templates \
                ../../Libs/libCC/cc64/qCC/libs/CCFbo \
                ../../Libs/libCC/cc64/libs/qCC_db \
                ../../Libs/libCC/cc64/libs/qCC_db/msvc \
                ../../Libs/libCC/cc64/libs/CCFbo/include \
                ../../Libs/boost_1_59_0_vs10_x64/include \
#                ../../Libs/PCL_vs10_x64/include/pcl-1.7 \
                ../../Libs/Eigen/include



debug{

                LIBS += -L$$PWD/../../Libs/libCC/build64/debug \
                    -L$$PWD/../../Libs/libCC/cc64/build/qCC/libs/CCFbo/Debug \
                    -L$$PWD/../../Libs/libCC/cc64/build/CC/Debug \
                    -L$$PWD/../../Libs/libCC/cc64/build/qCC/db/Debug \
                    -L$$PWD/../../Libs/libCC/cc64/build/libs/qCC_db/Debug \
                    -L$$PWD/../../Libs/libCC/cc64/build/CC/triangle/Debug \
                    -L$$PWD/../../Libs/libCC/cc64/build/libs/Glew/Debug \
                    -L$$PWD/../../Libs/libCC/cc64/build/libs/CCFbo/Debug
                LIBS += -L$$PWD/../../Libs/boost_1_59_0_vs10_x64/lib \
                    -L$$PWD/../../libs/libLAS/libLAS-1.8.0_x64/bin/Debug \

#                LIBS += -L$$PWD/../../Libs/PCL_vs10_x64/bin
#                LIBS += -L$$PWD/../../Libs/PCL_vs10_x64/lib
#                LIBS += -L$$PWD/../../Libs/Eigen/bin


                LIBS += -lcc -lQCC_DB_DLLd -lCC_DLLd -lCC_FBOd -lGLEWd -lliblas
#                LIBS += -lpcl_common_debug -lpcl_filters_debug -lpcl_io_debug -lpcl_kdtree_debug -lpcl_octree_debug -lpcl_sample_consensus_debug -lpcl_search_debug -lpcl_segmentation_debug

}else{
            LIBS += -L$$PWD/../../Libs/libCC/cc64/build/qCC/libs/CCFbo/Release \
                    -L$$PWD/../../Libs/libCC/cc64/build/CC/Release \
                    -L$$PWD/../../Libs/libCC/cc64/build/qCC/db/Release \
                    -L$$PWD/../../Libs/libCC/cc64/build/libs/qCC_db/Release \
                    -L$$PWD/../../Libs/libCC/cc64/build/CC/triangle/Release \
                    -L$$PWD/../../Libs/libCC/cc64/build/libs/Glew/Release \
                    -L$$PWD/../../Libs/libCC/cc64/build/libs/CCFbo/Release \
                    -L$$PWD/../../Libs/libCC/build64/release  \
                    -L$$PWD/../../libs/libLAS/libLAS-1.8.0_x64/bin/Release \

                LIBS += -L$$PWD/../../Libs/boost_1_59_0_vs10_x64/lib

#                LIBS += -L$$PWD/../../Libs/PCL_vs10_x64/bin
#                LIBS += -L$$PWD/../../Libs/PCL_vs10_x64/lib
#                LIBS += -L$$PWD/../../Libs/Eigen/bin

            LIBS += -lcc -lQCC_DB_DLL -lCC_DLL -lCC_FBO -lGLEW -lliblas
#            LIBS += -lpcl_common_release -lpcl_filters_release -lpcl_io_release -lpcl_kdtree_release -lpcl_octree_release -lpcl_sample_consensus_release -lpcl_search_release -lpcl_segmentation_release

}

RESOURCES += ../../Libs/libCC/cc/qCC/icones.qrc \
    solemapresources.qrc

RC_FILE = SolEMap.rc

