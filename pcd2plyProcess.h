#ifndef PCD2PLYPROCESS_H
#define PCD2PLYPROCESS_H

#include "ProcessManager/ExternalProcess.h"

class pcd2plyProcess : public ExternalProcess
{
public:
    pcd2plyProcess(QString inputFile);
    ~pcd2plyProcess();
};

#endif // PCD2PLYPROCESS_H
