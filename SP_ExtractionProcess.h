#ifndef SP_EXTRACTIONPROCESS_H
#define SP_EXTRACTIONPROCESS_H

#include "ProcessManager/ExternalProcess.h"


class SP_ExtractionProcess : public ExternalProcess
{
public:
    SP_ExtractionProcess(QString inputFile,float lat, float lon, float downsamplingDist, float distanceToRoofClustering, int remainingPointsToContinueSeg, float ransacDistanceToPlaneThreshold, int minInliersToAccpetPlane, float euclideanClusterToleranceToRemoveOutliers, float obstaclePerimeter);
    ~SP_ExtractionProcess();
private:
    bool removeDir(QString dirName);
};

#endif // SP_EXTRACTIONPROCESS_H
