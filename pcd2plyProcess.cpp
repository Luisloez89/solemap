#include "pcd2plyProcess.h"
#include "QDir"
pcd2plyProcess::pcd2plyProcess(QString inputFile):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/pcd2ply.exe")
{
    QString workingDir = QDir::currentPath()+"/Results";
    setWorkingDir(workingDir);
    setStartupMessage("Building ply files...");

    QFileInfo inputFileInfo(inputFile);
    QFileInfo outputFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+".ply");

    QStringList inputs;

    inputs << inputFileInfo.absoluteFilePath() << outputFile.absoluteFilePath();
    addIntputs(inputs);
}

pcd2plyProcess::~pcd2plyProcess(){

}

