#ifndef PMFPROCESS_H
#define PMFPROCESS_H

#include "ProcessManager/ExternalProcess.h"

class PMFProcess : public ExternalProcess
{
public:
    PMFProcess(QString inputFile,int maxWindowSize, float slope, float initialDistance,float maxDistance);
    ~PMFProcess();
};

#endif // PMFPROCESS_H
