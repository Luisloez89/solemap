/********************************************************************************
** Form generated from reading UI file 'MainWindowSoleMap.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOWSOLEMAP_H
#define UI_MAINWINDOWSOLEMAP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowSoleMap
{
public:
    QAction *action_Load_data;
    QAction *actionExit;
    QAction *actionRemove_Ground;
    QAction *actionAbout;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuHelp;
    QMenu *menuTools;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowSoleMap)
    {
        if (MainWindowSoleMap->objectName().isEmpty())
            MainWindowSoleMap->setObjectName(QString::fromUtf8("MainWindowSoleMap"));
        MainWindowSoleMap->resize(400, 300);
        action_Load_data = new QAction(MainWindowSoleMap);
        action_Load_data->setObjectName(QString::fromUtf8("action_Load_data"));
        actionExit = new QAction(MainWindowSoleMap);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionRemove_Ground = new QAction(MainWindowSoleMap);
        actionRemove_Ground->setObjectName(QString::fromUtf8("actionRemove_Ground"));
        actionAbout = new QAction(MainWindowSoleMap);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        centralWidget = new QWidget(MainWindowSoleMap);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        MainWindowSoleMap->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowSoleMap);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        MainWindowSoleMap->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowSoleMap);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindowSoleMap->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowSoleMap);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindowSoleMap->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(action_Load_data);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuHelp->addAction(actionAbout);
        menuTools->addAction(actionRemove_Ground);

        retranslateUi(MainWindowSoleMap);

        QMetaObject::connectSlotsByName(MainWindowSoleMap);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowSoleMap)
    {
        MainWindowSoleMap->setWindowTitle(QApplication::translate("MainWindowSoleMap", "MainWindowSoleMap", 0, QApplication::UnicodeUTF8));
        action_Load_data->setText(QApplication::translate("MainWindowSoleMap", "Load data", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindowSoleMap", "Exit", 0, QApplication::UnicodeUTF8));
        actionRemove_Ground->setText(QApplication::translate("MainWindowSoleMap", "Remove Ground", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("MainWindowSoleMap", "About", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindowSoleMap", "File", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindowSoleMap", "Help", 0, QApplication::UnicodeUTF8));
        menuTools->setTitle(QApplication::translate("MainWindowSoleMap", "Tools", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindowSoleMap: public Ui_MainWindowSoleMap {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOWSOLEMAP_H
