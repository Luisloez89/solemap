// Example program for the bounding_box() function for 2D points and 3D points.

#include <CGAL/Simple_cartesian.h>
#include <CGAL/bounding_box.h>
#include <CGAL/Aff_transformation_2.h>
#include <CGAL/Cartesian.h>
#include <CGAL/Iso_rectangle_2.h>
#include <CGAL/Largest_empty_iso_rectangle_2.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream> 
#define _USE_MATH_DEFINES
#include <math.h> 
#include <boost\filesystem.hpp>
#include <boost/foreach.hpp> 
#include <direct.h>

typedef double                                 Number_Type;
typedef CGAL::Cartesian<Number_Type>           K;
typedef CGAL::Aff_transformation_2<K> Transformation; 
typedef CGAL::Largest_empty_iso_rectangle_2<K> Largest_empty_iso_rect_2;
typedef K::Iso_rectangle_2                     Iso_rectangle_2;
typedef K::Point_2                 Point_2;
typedef K::Point_3                 Point_3;

using namespace std;



int main(int argc, char** argv)
{
std::string delimiter = " ";
    std::string line, solarIrr;
	
	
    mkdir("Results/Rectangles");
	mkdir("Results/SolarIrradiationPanel");
	mkdir("Results/PanelArea_m2");

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	boost::filesystem::path current_path( boost::filesystem::current_path() );
	std::stringstream pathCreator;
	pathCreator << current_path.string() << "/Results/ConcaveHulls";
	//std::cout << pathCreator.str();
	boost::filesystem::path pathBoost(pathCreator.str());
	//std::cout << pathBoost.string();
	boost::filesystem::directory_iterator it(pathBoost), eod;

	int contador =1;
	BOOST_FOREACH(boost::filesystem::path const &file, std::make_pair(it, eod))   
	{ 
		if(boost::filesystem::is_regular_file(file))
		{
			std::ifstream myfile (file.string());
			if (myfile.is_open())
			{
				std::vector<Point_2> concaveHull;
				std::vector<double> posmatrix(2);
				int aux_cont=0;
				getline(myfile,line);
				float orientation = std::atof(line.c_str());
				getline(myfile,line);
				float tilt = std::atof(line.c_str());
				if(std::abs(orientation)>=90) continue;
				std::cout << "Orientation: " << orientation << std::endl;
				while ( getline (myfile,line) )
				{
						size_t pos = 0;
						std::string token, token2;
						//int aux_posmatrix = 0;
						while ((pos = line.find(delimiter)) != std::string::npos) {
							token = line.substr(0, pos);
							posmatrix.at(0) = std::atof(token.c_str());
							line.erase(0, pos + delimiter.length());
							posmatrix.at(1) = std::atof(line.c_str());
							//aux_posmatrix++;
						}
					concaveHull.push_back(Point_2(posmatrix.at(0), posmatrix.at(1)));
					aux_cont++;
				}
				myfile.close();

				std::vector<Point_2> concaveHullRotated;

				//Rotate points
				Transformation rotate(CGAL::ROTATION, sin(orientation*M_PI/180), cos(orientation*M_PI/180));

				 //std::ofstream cloudRotated;
				 //cloudRotated.open ("cloudRotated.txt");
				for(int j =0; j<concaveHull.size();j++){
					concaveHullRotated.push_back(rotate(concaveHull[j])); 
					//cloudRotated << concaveHullRotated[j].x() << " " << concaveHullRotated[j].y() << std::endl;
				}
				//cloudRotated.close();

			  K::Iso_rectangle_2 bounding_box = CGAL::bounding_box(concaveHullRotated.begin(), concaveHullRotated.end());
			  std::cout <<"Bounding box: " << bounding_box << std::endl;

			  Transformation rotateBack(CGAL::ROTATION, sin(-orientation*M_PI/180), cos(-orientation*M_PI/180));

			    //// Inicio del calculo del largest_empty_iso_rectangle
			    int optimumSurfaceInRoof =0;
				float planarArea; 
				float tiltedArea;
			    do
				{
					optimumSurfaceInRoof++;

					Largest_empty_iso_rect_2 leir(bounding_box);
					leir.insert(concaveHullRotated.begin(), concaveHullRotated.end());
					Iso_rectangle_2 isoRectangleRotated = leir.get_largest_empty_iso_rectangle();

					planarArea = isoRectangleRotated.area(); 
					tiltedArea =((isoRectangleRotated.xmax()-isoRectangleRotated.xmin())*((isoRectangleRotated.ymax()-isoRectangleRotated.ymin())/std::cos(tilt*M_PI/180)));

					if(tiltedArea>=10 && (isoRectangleRotated.xmax()-isoRectangleRotated.xmin())>1 && (isoRectangleRotated.ymax()-isoRectangleRotated.ymin())>1){

						std::cout <<"puntos antes del relleno"<< concaveHullRotated.size() << std::endl;
						//Rellenar la superficie obtenida con puntos
						for(float i=isoRectangleRotated.xmin();i<isoRectangleRotated.xmax();i+=0.5){
							for(float j=isoRectangleRotated.ymin();j<isoRectangleRotated.ymax();j+=0.5){
								concaveHullRotated.push_back(Point_2(i,j));
							}

						}
						std::cout <<"puntos despues del relleno"<< concaveHullRotated.size() << std::endl;

						////////////////////////////////////


						std::vector<Point_2> isoRectangleFinal;
						isoRectangleFinal.push_back(rotateBack(Point_2(isoRectangleRotated.xmin(),isoRectangleRotated.ymin())));
						isoRectangleFinal.push_back(rotateBack(Point_2(isoRectangleRotated.xmin(),isoRectangleRotated.ymax())));
						isoRectangleFinal.push_back(rotateBack(Point_2(isoRectangleRotated.xmax(),isoRectangleRotated.ymin())));
						isoRectangleFinal.push_back(rotateBack(Point_2(isoRectangleRotated.xmax(),isoRectangleRotated.ymax())));

						//Writing Highest rectangle inscribed
						std::ofstream highestRectangle;
						std::stringstream rectFileName;

						rectFileName <<current_path.string()<< "/Results/Rectangles/" <<file.stem().string() <<"-"<<optimumSurfaceInRoof<< ".txt";// << "__Rect.txt";
			  
			  
						highestRectangle.open (rectFileName.str());
						for(int i = 0;i<isoRectangleFinal.size();i++){
						  highestRectangle << isoRectangleFinal[i].x() << " " << isoRectangleFinal[i].y() << std::endl;
						}
						highestRectangle.close();

						//Writing PanelSurfaceSolarIrr
						std::ofstream SolarIrradiationPanel;
						std::stringstream SolarIrradiationPanelFileName;

   						SolarIrradiationPanelFileName <<current_path.string()<< "/Results/SolarIrradiationPanel/" <<file.stem().string() <<"-"<<optimumSurfaceInRoof << ".txt";
			  
						std::stringstream SolarIrrPathCreator;
						SolarIrrPathCreator << current_path.string() << "/Results/SolarIrradiation/" << file.stem().string() << ".txt";
						//std::cout << pathCreator.str();
						boost::filesystem::path SolarIrradiation(SolarIrrPathCreator.str());
						std::ifstream SolarIrrFile (SolarIrradiation.string());
						float panelSolarIrr;
						if (SolarIrrFile.is_open()){
							getline(SolarIrrFile,solarIrr);
							panelSolarIrr = std::atof(solarIrr.c_str())*tiltedArea;
							std::cout << panelSolarIrr << std::endl;
						}
						//Write the solar irradiation data of the panel in the file
						std::cout << "solar panel irr path: "<<SolarIrradiationPanelFileName.str() << std::endl;

						SolarIrradiationPanel.open (SolarIrradiationPanelFileName.str());
						SolarIrradiationPanel << panelSolarIrr << std::endl;
						SolarIrradiationPanel.close();

		
						std::cout << "Planar area is " << isoRectangleRotated.area() << std::endl;
						std::cout << "Tilted area is " << tiltedArea << std::endl;

						///////////Write tilted and projected area
						
						std::ofstream SolarPanelArea_m2;
						std::stringstream SolarPanelArea_m2FileName;

   						SolarPanelArea_m2FileName <<current_path.string()<< "/Results/PanelArea_m2/" <<file.stem().string() <<"-"<<optimumSurfaceInRoof << ".txt";
			  

						SolarPanelArea_m2.open (SolarPanelArea_m2FileName.str());
						SolarPanelArea_m2 << isoRectangleRotated.area() << std::endl;
						SolarPanelArea_m2 << tiltedArea << std::endl;
						SolarPanelArea_m2.close();


					}else{
						break;
					}
				} while (true);///////////////////DANGER!!!
			}else{
						std::cout << "No se ha localizado el archivo" << std::endl;
				
			}

				
		} 
		contador++;
	}
	//std::system("pause");
	return 0;
	
	
}
