#include <iostream>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
typedef pcl::PointXYZI pointType;
using namespace std;


static void writeCloudToXYZRGBI_ASCII(string outputPath, pcl::PointCloud<pointType>::Ptr &cloud){

  ofstream myfile;
  myfile.open (outputPath);
  myfile << std::fixed;
  for (int i = 0; i < cloud->points.size(); ++i) {
       myfile << cloud->points.at(i).x <<" "<< cloud->points.at(i).y <<" "<<cloud->points.at(i).z<<" "<<cloud->points.at(i).intensity  <<"\n";
  }
  
  myfile.close();
  return;
}

int
 main (int argc, char** argv)
{
  pcl::PointCloud<pointType>::Ptr cloud (new pcl::PointCloud<pointType>);
  pcl::PointCloud<pointType>::Ptr cloud_filtered (new pcl::PointCloud<pointType>);

  string sourceFile = argv[1];
  string line;
  std::string delimiter = " ";
  ifstream myfile (sourceFile);
  float minZ =0.0;
  if (myfile.is_open())
  {
	pointType newPoint;
	
    while ( getline (myfile,line) )
    {
		std::string delimiters(" \t");
		std::vector<std::string> parts;
		boost::split(parts, line, boost::is_any_of(delimiters));
		newPoint.x =std::stod(parts[0]);
		newPoint.y =std::stod(parts[1]);
		newPoint.z =std::stod(parts[2]);
		newPoint.intensity =std::stod(parts[3]);
		cloud->points.push_back(newPoint);
		if(newPoint.z < minZ) minZ=newPoint.z;

    }
    myfile.close();
  }else{
	  cout << "Unable to open file"; 
	  return 1;
  }
  std::cerr << "Cloud before filtering: " << std::endl;
  std::cerr << *cloud << std::endl;

  // Create the filtering object
  pcl::PassThrough<pointType> pass;
  pass.setInputCloud (cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits(minZ,stof(argv[2]));
  pass.setFilterLimitsNegative (true);
  pass.filter (*cloud_filtered);

  std::cerr << "Ground cloud after filtering: " << std::endl;
  std::cerr << *cloud_filtered << std::endl;

  //pcl::PCDWriter writer;
  //writer.write<pointType> ("samp11-utm_ground.pcd", *cloud_filtered, false);
  boost::filesystem::path sourceFileBoost(sourceFile);
  string tartetfile =sourceFileBoost.parent_path().string();
  tartetfile.append("/roofs.asc");
  writeCloudToXYZRGBI_ASCII(tartetfile,cloud_filtered);

  
  std::cerr << "Roofs cloud after filtering: " << std::endl;
  std::cerr << *cloud_filtered << std::endl;

  return (0);
}