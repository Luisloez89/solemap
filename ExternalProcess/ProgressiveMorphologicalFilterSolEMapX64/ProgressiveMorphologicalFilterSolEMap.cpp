#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/progressive_morphological_filter.h>
#include <boost\filesystem.hpp>

typedef pcl::PointXYZI pointType;
using namespace std;


static void writeCloudToXYZRGBI_ASCII(string outputPath, pcl::PointCloud<pcl::PointXYZI>::Ptr &cloud){

  ofstream myfile;
  myfile.open (outputPath);
  myfile << std::fixed;
  for (int i = 0; i < cloud->points.size(); ++i) {
       myfile << cloud->points.at(i).x <<" "<< cloud->points.at(i).y <<" "<<cloud->points.at(i).z<<" "<<cloud->points.at(i).intensity  <<"\n";
  }
  
  myfile.close();
  return;
}

int
main (int argc, char** argv)
{
  pcl::PointCloud<pointType>::Ptr cloud (new pcl::PointCloud<pointType>);
  pcl::PointCloud<pointType>::Ptr cloud_filtered (new pcl::PointCloud<pointType>);
  pcl::PointIndicesPtr ground (new pcl::PointIndices);
  
  string sourceFile = argv[1];
  string line;
  std::string delimiter = " ";
  ifstream myfile (sourceFile);
  if (myfile.is_open())
  {
	pointType newPoint;
    while ( getline (myfile,line) )
    {
		std::string delimiters(" \t");
		std::vector<std::string> parts;
		boost::split(parts, line, boost::is_any_of(delimiters));
		newPoint.x =std::stod(parts[0]);
		newPoint.y =std::stod(parts[1]);
		newPoint.z =std::stod(parts[2]);
		newPoint.intensity =std::stod(parts[3]);
		cloud->points.push_back(newPoint);


    }
    myfile.close();
  }else{
	  cout << "Unable to open file"; 
	  return 1;
  }

  std::cerr << "Cloud before filtering: " << std::endl;
  std::cerr << *cloud << std::endl;

  // Create the filtering object
  pcl::ProgressiveMorphologicalFilter<pointType> pmf;
  pmf.setInputCloud (cloud);
  //pmf.setMaxWindowSize (20);
  //pmf.setSlope (1.0f);
  //pmf.setInitialDistance (0.5f);
  //pmf.setMaxDistance (3.0f);
  pmf.setMaxWindowSize (stoi(argv[2]));
  pmf.setSlope (stof(argv[3]));
  pmf.setInitialDistance (stof(argv[4]));
  pmf.setMaxDistance (stof(argv[5]));
  pmf.extract (ground->indices);

  // Create the filtering object
  pcl::ExtractIndices<pointType> extract;
  extract.setInputCloud (cloud);
  extract.setIndices (ground);
  extract.setNegative (true);
  extract.filter (*cloud_filtered);

  std::cerr << "Ground cloud after filtering: " << std::endl;
  std::cerr << *cloud_filtered << std::endl;

  //pcl::PCDWriter writer;
  //writer.write<pointType> ("samp11-utm_ground.pcd", *cloud_filtered, false);
  boost::filesystem::path sourceFileBoost(sourceFile);
  string tartetfile =sourceFileBoost.parent_path().string();
  tartetfile.append("/roofs.asc");
  writeCloudToXYZRGBI_ASCII(tartetfile,cloud_filtered);

  
  std::cerr << "Roofs cloud after filtering: " << std::endl;
  std::cerr << *cloud_filtered << std::endl;

  return (0);
}