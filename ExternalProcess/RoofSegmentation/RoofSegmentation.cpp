#include <iostream>
#include <direct.h>
#include <pcl/io/pcd_io.h>
#include <pcl\io\ply_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <boost\filesystem.hpp>
#include <boost/algorithm/string.hpp>
typedef pcl::PointXYZI pointType;
using namespace std;
namespace fs=boost::filesystem;

	float calOrientation(float x, float y){
		float orientation;
		float A = abs(atan(x/y)*(180/M_PI));
		if(x<0){
			if(y<0){
				orientation = A;
				return orientation;
			}else{
				orientation = 180-A;
				return orientation;
			}
		}else{
			if(y<0){
				orientation = -A;
				return orientation;
			}else{
				orientation = -180+A;
				return orientation;
			}
		}
	}

	float calInclin(float x, float y, float z){
		float distHor = sqrt(pow(x,2)+pow(y,2));
		float InclinacionNormal = atan(z/distHor)*(180/M_PI);
		return 90-InclinacionNormal;
	}

	float calPerdidas(float latitude, float orientation, float slope){
		float losses;
		if(slope>=15){
			losses= 100*((1.2/10000)*pow((slope-latitude+10),2)+(3.5/100000)*pow(orientation,2));
		}else{
			losses= 100*((1.2/10000)*pow((slope-latitude+10),2));
		}
		return losses;
	}

	double CalculateMean(pcl::PointCloud<pointType>::Ptr cloud)
    {
        double sum = 0;
        for(int i = 0; i < cloud->points.size(); i++)
            sum += cloud->points[i].intensity;
        return (sum / cloud->points.size());
    }
 
    double CalculateVariane(pcl::PointCloud<pointType>::Ptr cloud)
    {
        double mean = CalculateMean(cloud);
 
        double temp = 0;
        for(int i = 0; i < cloud->points.size(); i++)
        {
             temp += (cloud->points[i].intensity - mean) * (cloud->points[i].intensity - mean) ;
        }
        return temp / cloud->points.size();
    }
 
    double CalculateSampleVariane(pcl::PointCloud<pointType>::Ptr cloud)
    {
        double mean = CalculateMean(cloud);
 
        double temp = 0;
        for(int i = 0; i < cloud->points.size(); i++)
        {
             temp += (cloud->points[i].intensity - mean) * (cloud->points[i].intensity - mean) ;
        }
        return temp / (cloud->points.size() - 1);
    }
 
    double GetStandardDeviation(pcl::PointCloud<pointType>::Ptr cloud)
    {
        return sqrt(CalculateVariane(cloud));
    }
 
    double GetSampleStandardDeviation(pcl::PointCloud<pointType>::Ptr cloud)
    {
        return sqrt(CalculateSampleVariane(cloud));
    }

	void computeHorizontalConcaveHull(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud,int roof, int surface,float orientation, float tilt){
		std::cout << "Computing concave hull" << std::endl;
		pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudProjected (new pcl::PointCloud<pcl::PointXYZRGB>());
		pcl::copyPointCloud(*cloud,*cloudProjected);
	
		for(int i =0; i<cloudProjected->points.size();i++){
			cloudProjected->points[i].z=0;
		}
		//Get the convex hull
		pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_hull (new pcl::PointCloud<pcl::PointXYZRGB>());
		pcl::search::Search<pcl::PointXYZRGB>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZRGB> > (new pcl::search::KdTree<pcl::PointXYZRGB>);
		tree->setInputCloud(cloud);
		pcl::ConcaveHull<pcl::PointXYZRGB> chull;
	
		chull.setAlpha (0.2);

		chull.setInputCloud (cloudProjected);
		chull.setSearchMethod(tree);
		chull.reconstruct (*cloud_hull);

		std::cerr << "Concave hull has: " << cloud_hull->points.size ()
				<< " data points." << std::endl;
		std::stringstream name;
		name <<"Results/ConcaveHulls/Roof_"<< roof <<"_Surface_" << surface << ".txt";
		//name <<"Results/ConcaveHulls/Roof_"<< roof <<"_Surface_" << surface << "CloudConcave_hull" <<".txt";
	  std::ofstream concaveHullFile;
	  concaveHullFile.open (name.str());
	  concaveHullFile << orientation << std::endl;
	  concaveHullFile << tilt << std::endl;
	  for(int i = 1; i<cloud_hull->points.size();i++){
		concaveHullFile << cloud_hull->points[i].x << " " << cloud_hull->points[i].y << std::endl;
	  }
	  concaveHullFile.close();
	}

	void uniformDownsampling(pcl::PointCloud<pointType>::Ptr &cloud,pcl::PointCloud<pointType>::Ptr &cloudResampled,float distance){
		
		pcl::PointCloud<int> sampled_indices;
		std::cout << "Point cloud size before resamplig: " << cloud->points.size() << std::endl;
		pcl::UniformSampling<pointType> uniform_sampling;
		uniform_sampling.setInputCloud (cloud);
		uniform_sampling.setRadiusSearch (distance);
		uniform_sampling.compute (sampled_indices);
		pcl::copyPointCloud (*cloud, sampled_indices.points, *cloudResampled);
		std::cout << "Point cloud size after resamplig (10cm): " << cloudResampled->points.size() << std::endl << std::endl;
	}

	void roofSegmentation(pcl::PointCloud<pointType>::Ptr &cloud, std::vector<pcl::PointIndices> &eachRoof_indices, float distance){
		pcl::search::KdTree<pointType>::Ptr tree (new pcl::search::KdTree<pointType>);
		tree->setInputCloud(cloud);
		std::cout << "Initiating Roof segmentation (Euclidean cluster segmentation)" << std::endl;
		//Euclidean cluster segmentation to remove outliers
		pcl::EuclideanClusterExtraction<pointType> ec;
		ec.setClusterTolerance (distance);
		ec.setSearchMethod (tree);
		ec.setInputCloud (cloud);
		ec.extract (eachRoof_indices);
	}

	void getRoofCluster(pcl::PointCloud<pointType>::Ptr &cloud, boost::shared_ptr<vector<int>> &indicesptr, pcl::PointCloud<pointType>::Ptr &roof_cluster){
		pcl::ExtractIndices<pointType> eifilter (true); // Initializing with true will allow us to extract the removed indices
		eifilter.setInputCloud (cloud);
		eifilter.setIndices(indicesptr);
		eifilter.filter (*roof_cluster);
	}

	void computeOrientationAndInclination(Eigen::VectorXf &coeficientes,float &Or, float &In){
		if(coeficientes[2]<0){
					std::cout << "Plane coeficcients: " << -coeficientes[0] << "   " << -coeficientes[1] << "   " << -coeficientes[2] << std::endl;
					Or = calOrientation(-coeficientes[0],-coeficientes[1]);
					In = calInclin(-coeficientes[0],-coeficientes[1],-coeficientes[2]);
		}else{
					std::cout << "Plane coeficcients: " << coeficientes[0] << "   " << coeficientes[1] << "   " << coeficientes[2] << std::endl;
					Or = calOrientation(coeficientes[0],coeficientes[1]);
					In = calInclin(coeficientes[0],coeficientes[1],coeficientes[2]);
		}
	}

	void extractPlaneRANSAC(pcl::PointCloud<pointType>::Ptr &roof_cluster, Eigen::VectorXf &coeficientes, std::vector<int> &inliers, float threshold){
		pcl::SampleConsensusModelPlane<pointType>::Ptr model_p (new pcl::SampleConsensusModelPlane<pointType> (roof_cluster));
		pcl::RandomSampleConsensus<pointType> ransac (model_p);
		ransac.setDistanceThreshold (threshold);
		//ransac.setMaxIterations(1000);
		ransac.computeModel();
		ransac.getInliers(inliers);
	
		if (inliers.size () < 2000)
		{
			std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
			return;
		}
		ransac.getModelCoefficients(coeficientes);
	
	}

	
//Falta diseñar un metodo para parar la ejecucion cuando queden puntos y no pueda extraer ningun plano mas
int
main (int argc, char** argv)
{
	fs::path resultsPath("Results");
	if(fs::is_directory(resultsPath)){
		for (fs::directory_iterator end_dir_it, it(resultsPath); it!=end_dir_it; ++it) {
		remove_all(it->path());
		}
	}
	mkdir("Results");
	mkdir("Results/RoofSegmentationClouds");
	mkdir("Results/RoofSegmentationCloudsWithoutObstacles");
	mkdir("Results/RoofSegmentationInfo");
	mkdir("Results/AuxiliarData");
	mkdir("Results/ConcaveHulls");
	mkdir("Results/SolarIrradiation");
	
	const clock_t begin_time = clock();
	bool PlaneExtractionFail;
	int PlaneExtractedFailCounter;
	int roofsSuitableWithoutSupports=0;
	int roofsSuitableWithSupports=0;
	int roofsNotSuitable=0;

  //pcl::PCDWriter writer;
  pcl::PLYWriter writer;
  pcl::ExtractIndices<pointType> extract;

  pcl::PointCloud<pointType>::Ptr cloud (new pcl::PointCloud<pointType>);
  

  //if ( pcl::io::loadPCDFile <pointType> ("DetailAngled1Roof.pcd", *cloud) == -1)
  //if ( pcl::io::loadPCDFile <pointType> (string (argv[1]), *cloud) == -1)
  //{
  //  std::cout << "Cloud reading failed." << std::endl;
  //  return (-1);
  //}
  string line;
  std::string delimiter = " ";
  ifstream myfile (argv[1]);
  if (myfile.is_open())
  {
	pcl::PointXYZI newPoint;
    while ( getline (myfile,line) )
    {
		std::string delimiters(" \t");
		std::vector<std::string> parts;
		boost::split(parts, line, boost::is_any_of(delimiters));
		newPoint.x =std::stod(parts[0]);
		newPoint.y =std::stod(parts[1]);
		newPoint.z =std::stod(parts[2]);
		newPoint.intensity =std::stod(parts[3]);
		cloud->points.push_back(newPoint);


    }
    myfile.close();
  }else{
	  cout << "Unable to open file"; 
	  return 1;
  }

	std::cout << "Point cloud loaded" << std::endl;

	//Results file
	ofstream resultsFile;
	resultsFile.open ("Results/GeneralResults.txt");
	resultsFile << "Results for the process of the cloud "+string (argv[1])+"\n\n";
	//resultsFile << "Results for the process of the cloud \n\n";

    //Uniform sampling
    pcl::PointCloud<pointType>::Ptr cloudResampled (new pcl::PointCloud<pointType>);
	std::cout << "Starting uniform downsampling" << std::endl;
	float downsamplingDistance = atof(argv[4]);
	uniformDownsampling(cloud,cloudResampled,downsamplingDistance);
    
	//Segmentación de cada tejado
	std::vector<pcl::PointIndices> eachRoof_indices;
	float distanceToRoofClustering = atof(argv[5]);
	roofSegmentation(cloudResampled,eachRoof_indices,distanceToRoofClustering); ///Con 2 funciona bien
	std::cout << "Roof segmentation finished. Roofs extracted: " << eachRoof_indices.size() << std::endl <<std::endl; 

	///Visualización de euclidean Cluster extraction
	/*boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->setBackgroundColor (255, 255, 255);
	viewer->addCoordinateSystem (1.0, "global");
	viewer->initCameraParameters ();*/
  
	std::cout << "Initiating surface segmentation (RANSAC)." << std::endl << std::endl;

	for(int k =0;k<eachRoof_indices.size();k++){

			//Variable que indica el numero del plano extraido
			int i =1;
			std::cout << "processing roof " << k+1 << " of " << eachRoof_indices.size() << std::endl;

			//Get the roof cluster
			pcl::PointCloud<pointType>::Ptr roof_cluster (new pcl::PointCloud<pointType>);
			boost::shared_ptr<vector<int> > indicesptr (new vector<int>(eachRoof_indices[k].indices)); 
			getRoofCluster(cloudResampled,indicesptr,roof_cluster);

		/////////////////////////////////////////// Visualizar Resultados del euclidean cluster
			 /*pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (roof_cluster,rand() % 256   , rand() % 256, rand() % 256);
				viewer->addPointCloud<pointType> (roof_cluster,single_color, "Roof_"+k+1);
				viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "Roof_"+k+1);*/
			////////////////////////////////////////

		int totalInitialPoints = roof_cluster->points.size(); 
		//variable para cambiar de tejado cuando no pueda extraermas tejados.

		//PLANES SEGMENTATION
		PlaneExtractedFailCounter=0;
		//Realizamos segmentación mientras queden mas del 10% de los puntos
			float remainingPointsToContinueSegmentation = atof(argv[6]);
			while(roof_cluster->points.size()>totalInitialPoints*remainingPointsToContinueSegmentation){
				PlaneExtractionFail=true;
					if(PlaneExtractedFailCounter>=3){
						break;
					}
				pcl::PointCloud<pointType>::Ptr cloudAux (new pcl::PointCloud<pointType>);  
				cloudAux->clear();
				Eigen::VectorXf coeficientes;
				std::vector<int> inliers;
				float ransacDistanceToPlaneThreshold=atof(argv[7]);
				extractPlaneRANSAC(roof_cluster, coeficientes, inliers,ransacDistanceToPlaneThreshold);
				//pcl::SampleConsensusModelPlane<pointType>::Ptr model_p (new pcl::SampleConsensusModelPlane<pointType> (roof_cluster));
				//pcl::RandomSampleConsensus<pointType> ransac (model_p);
				//ransac.setDistanceThreshold (.1);
				////ransac.setMaxIterations(1000);
				//ransac.computeModel();
				//
				//ransac.getInliers(inliers);
	
				//if (inliers.size () < 2000)
				//{
				//	std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
				//	break;
				//}

				//
				//ransac.getModelCoefficients(coeficientes);

				int minPointsToAcceptRANSACPlane = atof(argv[8]);
				if (inliers.size () < minPointsToAcceptRANSACPlane)
				{
					std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
					break;
				}
	
				float orientation;
				float inclination;
				computeOrientationAndInclination(coeficientes,orientation,inclination);

	
				//float lat = 40.66;
				//float lon = -4.70;
				float lat =atof(argv[2]);
				float lon =atof(argv[3]);
				float Losses = calPerdidas(lat,orientation,inclination);
				std::cout << "Orientation: " << orientation<< std::endl;
				std::cout << "Tilt: " << inclination<< std::endl;
				std::cout << "Losses: " << Losses<< std::endl;


				// copies all inliers of the model computed to another PointCloud
				pcl::PointCloud<pointType>::Ptr CloudPlanarRansac (new pcl::PointCloud<pointType>);
	
				boost::shared_ptr<vector<int> > indicesptr (new vector<int>(inliers)); 
				pcl::ExtractIndices<pointType> eifilter (true); // Initializing with true will allow us to extract the removed indices
				eifilter.setInputCloud (roof_cluster);
				eifilter.setIndices(indicesptr);
				eifilter.filter (*CloudPlanarRansac);
				std::cerr << "PointCloud representing the planar component: " << inliers.size() << " data points." << std::endl;

				// The indices_rem array indexes all points of cloud_in that are not indexed by indices_in
				eifilter.setNegative (true);
				eifilter.filter (*cloudAux);
	
				//writer.writeBinary<pointType> ("CloudPlanarRansac.pcd", *CloudPlanarRansac);

				// Creating the KdTree object for the search method of the extraction
				pcl::search::KdTree<pointType>::Ptr tree (new pcl::search::KdTree<pointType>);
				tree->setInputCloud (CloudPlanarRansac);

				//Euclidean cluster segmentation to remove outliers
				std::vector<pcl::PointIndices> cluster_indices;
				pcl::EuclideanClusterExtraction<pointType> ec;
				float euclideanClusterToleranceToRemoveOutliers =atof(argv[9]);
				ec.setClusterTolerance (euclideanClusterToleranceToRemoveOutliers);
				ec.setSearchMethod (tree);
				ec.setInputCloud (CloudPlanarRansac);
				ec.extract (cluster_indices);

	
				for(int j =0;j<cluster_indices.size();j++){
					pcl::PointCloud<pointType>::Ptr cloud_cluster (new pcl::PointCloud<pointType>);

					boost::shared_ptr<vector<int> > indicesptr (new vector<int>(cluster_indices[j].indices)); 
					pcl::ExtractIndices<pointType> eifilter (true); // Initializing with true will allow us to extract the removed indices
					eifilter.setInputCloud (CloudPlanarRansac);
					eifilter.setIndices(indicesptr);
					eifilter.filter (*cloud_cluster);

					if(cloud_cluster->points.size()>minPointsToAcceptRANSACPlane){ // 3000 as default value ATENCION!!!
						//Estatistical outliers removal;
						pcl::PointCloud<pointType>::Ptr cloud_withoutOutliers (new pcl::PointCloud<pointType>);
						// Create the filtering object
						pcl::StatisticalOutlierRemoval<pointType> sor;
						sor.setInputCloud (cloud_cluster);
						sor.setMeanK (50);
						sor.setStddevMulThresh (1.0);
						sor.filter (*cloud_withoutOutliers);
						std::stringstream sRepr;
						sRepr << "sample cloud_"<<k<<"_Surface_" << i;
						//Visualizar resultados de RANSAC
						/*pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (roof_cluster,rand() % 256   , rand() % 256, rand() % 256);
						viewer->addPointCloud<pointType> (cloud_cluster,single_color, sRepr.str());
						viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, sRepr.str());*/
						/* if(Losses<=5){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,0, 255, 0);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else if(Losses<=10){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,36, 255, 42);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else if(Losses<=15){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,72, 255, 39);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else if(Losses<=20){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,109, 255, 43);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else if(Losses<=25){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,145, 255, 50);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else if(Losses<=30){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,182, 255, 0);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else if(Losses<=35){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,218, 255, 0);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else if(Losses<=40){
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,255, 255, 0);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}else{
						pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,255, 0, 0);
						viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
						}*/
				//Remove points by thermography
				double meanTemperature = CalculateMean(cloud_withoutOutliers);
				double stdDeviationTemperature = GetStandardDeviation(cloud_withoutOutliers);

				pcl::PointIndices removedIndicesThermography;
				pcl::PointCloud<pointType>::Ptr cloud_cluster_AfterThemoAnalisis (new pcl::PointCloud<pointType>);
				//pcl::PointCloud<pointType>::Ptr cloud_cluster_AfterThemoAnalisis_RemovedPoints (new pcl::PointCloud<pointType>);


				////// Le aplico el color correspondiente a orientación e inclinación.
				pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster_Classified (new pcl::PointCloud<pcl::PointXYZRGB>);
				pcl::copyPointCloud(*cloud_withoutOutliers,*cloud_cluster_Classified);
				if(abs(orientation)>80){
							for(int t =0; t<cloud_cluster_Classified->points.size();t++){
								//Orientación Norte color Rojo
								cloud_cluster_Classified->points[t].r=255;
								cloud_cluster_Classified->points[t].g=0;
								cloud_cluster_Classified->points[t].b=0;
							}
						}
					 
					  if(abs(orientation)<=80){
						
						  if(Losses>20){
							for(int t =0; t<cloud_cluster_Classified->points.size();t++){
								//Compatible con soportes. color amarillo
								cloud_cluster_Classified->points[t].r=255;
								cloud_cluster_Classified->points[t].g=255;
								cloud_cluster_Classified->points[t].b=0;
							}
						  }else{
							for(int t =0; t<cloud_cluster_Classified->points.size();t++){
								//Compatible sin soportes. color verde
								cloud_cluster_Classified->points[t].r=0;
								cloud_cluster_Classified->points[t].g=255;
								cloud_cluster_Classified->points[t].b=0;
							}
						  }
					   }

				///
				int pointsRemovedThermo=0;

				for(int l = 0;l<cloud_withoutOutliers->points.size(); l++){
					if(abs((cloud_withoutOutliers->points[l].intensity)-meanTemperature)>(stdDeviationTemperature+1)){
						
						removedIndicesThermography.indices.push_back(l);
						pointsRemovedThermo++;
					}
				}
				pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRoofWithoutObstacles (new pcl::PointCloud<pcl::PointXYZRGB>);
				pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRoofWithoutObstaclesFinal (new pcl::PointCloud<pcl::PointXYZRGB>);
				pcl::copyPointCloud(*cloud_cluster_Classified,*cloudRoofWithoutObstacles);

				//Search NearestPoint with kdtree
				for(int m=0;m<removedIndicesThermography.indices.size();m++){
					// Search parameters
					float obstaclePerimeter = atof(argv[10]);
					const float k = obstaclePerimeter; //Value of the perimeter
					std::vector<int> pointIdxRadiusSearch;
					std::vector<float> pointRadiusSquaredDistance;
					// Run search
					pcl::KdTreeFLANN<pointType> search;
					search.setInputCloud (cloud_withoutOutliers);
					if(search.radiusSearch(removedIndicesThermography.indices[m],k,pointIdxRadiusSearch,pointRadiusSquaredDistance)>0){
						//std::cout << "Puntos a una distancia meor de 0.2m del punto " <<removedIndicesThermography.indices[m]<<std::endl;
						for(int t=0;t<pointIdxRadiusSearch.size();t++){
							//std::cout << "Point: " << pointIdxRadiusSearch[t] << " Distance: " << pointRadiusSquaredDistance[t]<<std::endl;
							//cloud_withoutOutliers->points[pointIdxRadiusSearch[t]].intensity=50;
							cloud_cluster_Classified->points[pointIdxRadiusSearch[t]].r=128;
							cloud_cluster_Classified->points[pointIdxRadiusSearch[t]].g=128;
							cloud_cluster_Classified->points[pointIdxRadiusSearch[t]].b=128;
							cloudRoofWithoutObstacles->points[pointIdxRadiusSearch[t]].x=0;
						}
					}
					
				}
				for(int m=0;m<removedIndicesThermography.indices.size();m++){
					cloud_cluster_Classified->points[removedIndicesThermography.indices[m]].r=0;
					cloud_cluster_Classified->points[removedIndicesThermography.indices[m]].g=0;
					cloud_cluster_Classified->points[removedIndicesThermography.indices[m]].b=0;
					cloudRoofWithoutObstacles->points[removedIndicesThermography.indices[m]].x=0;
				}

				for(int contad=0;contad<cloudRoofWithoutObstacles->points.size();contad++){
					if(cloudRoofWithoutObstacles->points[contad].x!=0){
						cloudRoofWithoutObstaclesFinal->points.push_back(cloudRoofWithoutObstacles->points[contad]);
					}
				}

						/*std::stringstream ssRoofWithoutObstaclesFinal;
						ssRoofWithoutObstaclesFinal << "Roof_"<< k+1 <<"_SurfaceWithoutObstacles_" << i <<"_final";*/
						
						

						//std::cout << "POINTS REMOVED FOR THERMO: "<< pointsRemovedThermo << std::endl;

						PlaneExtractionFail=false;
						std::cout << "Writing point cloud " << i << " of the roof "<< k+1<< std::endl;
						std::stringstream ss;
						ss << "Roof_"<< k+1 <<"_Surface_" << i;
						//writer.writeBinary<pcl::PointXYZRGB> ("Results/RoofSegmentationClouds/"+ss.str ()+".pcd", *cloud_cluster_Classified);
						//writer.writeBinary<pcl::PointXYZRGB> ("Results/RoofSegmentationCloudsWithoutObstacles/"+ss.str ()+".pcd", *cloudRoofWithoutObstaclesFinal);
						writer.write ("Results/RoofSegmentationClouds/"+ss.str ()+".ply", *cloud_cluster_Classified,true,false);
						writer.write ("Results/RoofSegmentationCloudsWithoutObstacles/"+ss.str ()+".ply", *cloudRoofWithoutObstaclesFinal,true,false);
						
						//Compute and write concaveHull
						computeHorizontalConcaveHull(cloudRoofWithoutObstaclesFinal,k+1,i,orientation,inclination);

						//writer.writeBinary<pointType> ("b__"+ss.str ()+".pcd", *cloud_withoutOutliers); 
						//writer.writeBinary<pointType> ("a__"+ss.str ()+"_WithoutThermo.pcd", *cloud_cluster_AfterThemoAnalisis); 
			
						//Writing data of the point cloud
						std::cout << "Writing data  point cloud " << i << " of the roof "<< k+1<< std::endl;
						std::stringstream sText, sTextAuxiliarData;
						sText << "Results/RoofSegmentationInfo/Roof_"<< k+1 <<"_Surface_" << i << ".txt";
						sTextAuxiliarData << "Results/AuxiliarData/Auxiliar_Roof_"<< k+1 <<"_Surface_" << i << ".txt";
						ofstream myfile, myFileAuxiliarData;
						myfile.open (sText.str());
						myFileAuxiliarData.open(sTextAuxiliarData.str());
						myFileAuxiliarData << lat << std::endl;
						myFileAuxiliarData << lon << std::endl;
						myfile << "Roof_"<< k+1 <<"_Surface_" << i;
						if(abs(orientation)>80){
							myfile << "  ----   ROOF WITHOUT SOUTH ORIENTATION\n";
							myFileAuxiliarData << "Null";
							resultsFile << ss.str() << " --> NOT SUITABLE FOR THE INSTALLATION OF SOLAR PANELS\n";
							roofsNotSuitable++;
							/*pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,255, 0, 0);
							viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
							viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, sRepr.str());*/
						   }else{
						   myfile << "\n";
						   }
						myfile << "Point cloud with "<< cloud_withoutOutliers->points.size()<< " points\n";
			 

						myfile << "Orientation: " << orientation << "º\n";
						myFileAuxiliarData << orientation << std::endl;
						myfile << "Slope: " << inclination << "º\n";
						myFileAuxiliarData << inclination << std::endl;
						if(abs(orientation)<=80){
							myfile << "Losses by inclination and orientation: " << Losses << "%\n";
							if(Losses>20){
								myfile << "Ideal inclination for the support structure: " << lat-inclination << "º\n";
								resultsFile << ss.str() << " --> ROOF SUITABLE FOR THE INSTALLATION OF SOLAR PANELS IN A SUPPORT OF "<< lat-inclination << "º\n";
								roofsSuitableWithSupports++;
								/*pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,255, 255, 0);
								viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
								viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, sRepr.str());*/
							}else{
								resultsFile << ss.str() << " --> ROOF SUITABLE FOR THE INSTALLATION OF SOLAR PANELS WITHOUT SUPPORT\n";
								roofsSuitableWithoutSupports++;
								/*pcl::visualization::PointCloudColorHandlerCustom<pointType> single_color (cloud_withoutOutliers,0, 255, 0);
								viewer->addPointCloud<pointType> (cloud_withoutOutliers,single_color, sRepr.str());
								viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, sRepr.str());*/
							}
						}

						myfile.close();
						myFileAuxiliarData.close();

						//Compute solar irradiation:
						std::stringstream irradiationExe;
						float azimutToSolarIrr;
						if(orientation>=0){
						azimutToSolarIrr = orientation+180;
						}else{
						azimutToSolarIrr=180-orientation;
						}
						irradiationExe << "computeSolarIrradiation.exe -lat " << lat << " -lon " << lon << " -az " << azimutToSolarIrr << " -t "<< inclination << " -sc 082100 -name Roof_"<< k+1 <<"_Surface_" << i << ".txt";
						std::string irradiationExeString = irradiationExe.str();
						system(irradiationExeString.c_str());
						i++;
						}else{
							*cloudAux += *cloud_cluster;
						}
					if(PlaneExtractionFail){
						PlaneExtractedFailCounter++;
					}
				}

				roof_cluster->clear();
				roof_cluster=cloudAux;
			}

		}
	
	/*while (!viewer->wasStopped())
  {
    viewer->spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds(100000));
  }*/

	resultsFile << "\n\n";
	resultsFile << "Roofs suitable for the installation of solar panels WITHOUT supports: "<<roofsSuitableWithoutSupports<<"\n";
	resultsFile << "Roofs suitable for the installation of solar panels WITH supports: "<<roofsSuitableWithSupports<<"\n";
	resultsFile << "Roofs NOT suitable for the installation of solar panels : "<<roofsNotSuitable<<"\n";
	resultsFile << "Total planes segmented: " << roofsSuitableWithoutSupports+roofsSuitableWithSupports+roofsNotSuitable << std::endl;
	resultsFile.close();


	std::cout << "Start SearchArea_ForFolder.exe" <<std::endl;
	system("SearchArea_ForFolder.exe");
	std::cout << "Start InliersHiRect.exe" <<std::endl;
	system("InliersHiRect.exe");
	//std::cout << "Start ColorizeSolarPanelSurfaces.exe" <<std::endl;
	//system("ColorizeSolarPanelSurfaces.exe");


	std::cout << "Execution time: "<< float( clock () - begin_time ) /  CLOCKS_PER_SEC;
	//system("pause");
	return 0;
}

