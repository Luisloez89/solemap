#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/filters/crop_hull.h>
typedef pcl::PointXYZ pointType;
#include <direct.h>
#include <boost\filesystem.hpp>
#include <boost/foreach.hpp> 

using namespace std;

int
main (int argc, char** argv)
{
	float totalArea=0; //variable para obtener el area total de todos los paneles
	double totalSolarIrr =0; //variable para obtener la irradiación solar total recibida por todos los paneles

	mkdir("Results/PanelAreas");
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	boost::filesystem::path current_path( boost::filesystem::current_path() );
	std::stringstream cloudsPathCreator;
	cloudsPathCreator << current_path.string() << "/Results/RoofSegmentationCloudsWithoutObstacles";
	std::stringstream rectanglesPathCreator;
	rectanglesPathCreator << current_path.string() << "/Results/Rectangles";

	

	boost::filesystem::path pathBoostClouds(cloudsPathCreator.str());
	boost::filesystem::path pathBoostRect(rectanglesPathCreator.str());
	//boost::filesystem::directory_iterator it(pathBoostClouds), eod;
	boost::filesystem::directory_iterator it(pathBoostRect), eod;

	int contador =1;
	BOOST_FOREACH(boost::filesystem::path const &file, std::make_pair(it, eod))   
	{ 
		if(boost::filesystem::is_regular_file(file))
		{
				  pcl::PointCloud<pointType>::Ptr cloud (new pcl::PointCloud<pointType>());
				  pcl::PointCloud<pointType>::Ptr hiRectCloud (new pcl::PointCloud<pointType>());

				  //if ( pcl::io::loadPCDFile <pointType> (string (argv[1]), *cloud) == -1)
				  //Get the corrrespondent cloud
				  std::string s = file.string();
				  std::string delimiter = "-";
				  std::stringstream containerCloud;
			      containerCloud << pathBoostClouds.string() << "/" << file.stem().string().substr(0, file.stem().string().find(delimiter)) << ".ply";// << "__Rect.txt";

				  std::cout << "file being evaluated "<< file.string()<<std::endl;
				  std::cout << "Container Cloud: "<<containerCloud.str() <<std::endl;
				  if ( pcl::io::loadPLYFile (containerCloud.str(), *cloud) != -1)
				  {
					    std::string delimiter = " ";
						std::string line;
						std::stringstream rectFilePath;
						rectFilePath << pathBoostRect.string() << "/" << file.stem().string()<<".txt";// << "__Rect.txt";
						string prueba = rectFilePath.str();
						std::ifstream myfile (rectFilePath.str());
						//std::ifstream myfile (argv[1]);
						if (myfile.is_open())
						{
						std::vector<double> posmatrix(2);
							int aux_cont=0;
							//getline(myfile,line);
							//float orientation = std::atof(line.c_str());
							//std::cout << "Orientation: " << orientation << std::endl;
							while ( getline (myfile,line) )
							{
									size_t pos = 0;
									std::string token, token2;
									//int aux_posmatrix = 0;
									while ((pos = line.find(delimiter)) != std::string::npos) {
										token = line.substr(0, pos);
										posmatrix.at(0) = std::atof(token.c_str());
					
										line.erase(0, pos + delimiter.length());
										posmatrix.at(1) = std::atof(line.c_str());
										//aux_posmatrix++;
									}
								hiRectCloud->points.push_back(pointType(posmatrix.at(0), posmatrix.at(1),0));
								hiRectCloud->points.push_back(pointType(posmatrix.at(0), posmatrix.at(1),1500));
								aux_cont++;
							}
							myfile.close();
						}
						for(int i = 0;i<hiRectCloud->points.size();i++){
							std::cout << hiRectCloud->points[i] << std::endl;
						}

						pcl::ConvexHull<pointType> hull;
						hull.setInputCloud(hiRectCloud);
						hull.setDimension(3);
						std::vector<pcl::Vertices> polygons;

				      pcl::PointCloud<pointType>::Ptr surface_hull (new pcl::PointCloud<pointType>);
				      hull.reconstruct(*surface_hull, polygons);

					  pcl::PointCloud<pointType>::Ptr objects (new pcl::PointCloud<pointType>);
					  pcl::CropHull<pointType> bb_filter;

					  bb_filter.setDim(3);
					  bb_filter.setInputCloud(cloud);
					  bb_filter.setHullIndices(polygons);
					  bb_filter.setHullCloud(surface_hull);
					  bb_filter.filter(*objects);

					  std::cout << cloud->size() << std::endl;
					  std::cout << objects->size() << std::endl;

					  if(objects->size()>100){
					  std::stringstream fileName;
					  /*fileName <<current_path.string()<< "/Results/PanelAreas/" << file.stem().string() << ".pcd";*/
					  fileName <<current_path.string()<< "/Results/PanelAreas/" << file.stem().string() << ".ply";
					  std::cout << fileName.str()<< std::endl;
					  //pcl::io::savePCDFileBinaryCompressed (fileName.str(), *objects);
					  //pcl::io::savePLYFileBinary(fileName.str(), *objects);
					  pcl::PLYWriter writer;
				      writer.write (fileName.str(), *objects,true,false);

							  ///Compute the total area of the panels
							  std::stringstream areaFileName;
							  areaFileName <<current_path.string()<< "/Results/PanelArea_m2/" << file.stem().string() << ".txt";

							  std::ifstream areafile (areaFileName.str());
							  std::string lineArea;
								if (areafile.is_open())
								{
									getline(areafile,lineArea);
									getline(areafile,lineArea);
									totalArea +=std::atof(lineArea.c_str());
									areafile.close();
								}

								///Compute the total solarIrradiation of the panels
							  std::stringstream SolarIrrFileName;
							  SolarIrrFileName <<current_path.string()<< "/Results/SolarIrradiationPanel/" << file.stem().string() << ".txt";

							  std::ifstream SolarIrrfile (SolarIrrFileName.str());
							  std::string lineSolarIrr;
								if (SolarIrrfile.is_open())
								{
									getline(SolarIrrfile,lineSolarIrr);
									totalSolarIrr +=std::atof(lineSolarIrr.c_str());
									SolarIrrfile.close();
								}

					  }else{
					  std::stringstream fileName;
					  fileName <<current_path.string()<< "/Results/SolarIrradiationPanel/" << file.stem().string() << ".txt";
					  boost::filesystem::wpath fileToRemove(fileName.str());
						if(boost::filesystem::exists(fileToRemove))
								boost::filesystem::remove(fileToRemove);
					  }
					  
				  }
		} 
		contador++;
	}

	std::cout << "Area Total: "<< totalArea<<std::endl;
	std::cout << "Irradiación solar total recibida: " << totalSolarIrr << std::endl;
	/////////////////////////////////////////////////////////////////////////////
	//system("pause");
  return 0;
}