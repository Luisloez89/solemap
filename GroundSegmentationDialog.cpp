#include "GroundSegmentationDialog.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QDesktopWidget>
#include "PMFProcess.h"
#include "PassThroughFilterProcess.h"
#include <QFileInfo>
#include <QMessageBox>
GroundSegmentationDialog::GroundSegmentationDialog(QWidget *parent,QString sourceFile) :
    QDialog(parent),mSourceFile(sourceFile)
{
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QRect rec = QApplication::desktop()->screenGeometry();

        setMinimumSize(rec.width()*0.6, rec.height()*0.6);
        QHBoxLayout *mainLayout = new QHBoxLayout();
        QFrame *confFrame= new QFrame();
        confFrame->setMaximumWidth(300);
        QVBoxLayout *confFrameLayout= new QVBoxLayout();
        confFrameLayout->setContentsMargins(0,0,0,0);

        QFrame *methodFrame = new QFrame();
        QHBoxLayout *methodFrameLayout = new QHBoxLayout();
        methodFrameLayout->setContentsMargins(0,0,0,0);

        methodFrameLayout->addWidget(new QLabel("Segmentation methond"));
        methodFrameLayout->addWidget(mCbMethod = new QComboBox());
        mCbMethod->addItem("passthrough filter");
        mCbMethod->addItem("Progressive morphological filter");
        connect(mCbMethod,SIGNAL(currentIndexChanged(int)),this,SLOT(on_CbMethodSelectionChanged(int)));
        methodFrame->setLayout(methodFrameLayout);
        confFrameLayout->addWidget(methodFrame);

        mPMFsettingsFrame = new QFrame();
        QGridLayout *PMFsettingsFrameLayout = new QGridLayout();
        PMFsettingsFrameLayout->setContentsMargins(0,0,0,0);

        PMFsettingsFrameLayout->addWidget(new QLabel("Max windows size"),0,0);
        PMFsettingsFrameLayout->addWidget(mMaxWindowSize = new QSpinBox(),0,1);

        mMaxWindowSize->setRange(10,50);
        mMaxWindowSize->setValue(20);
        mMaxWindowSize->setSingleStep(1);


        PMFsettingsFrameLayout->addWidget(new QLabel("Slope"),1,0);
        PMFsettingsFrameLayout->addWidget(mSlope = new QDoubleSpinBox(),1,1);

        mSlope->setRange(0.1,10.0);
        mSlope->setValue(1.0);
        mSlope->setSingleStep(0.1);

        PMFsettingsFrameLayout->addWidget(new QLabel("Initial distance"),2,0);
        PMFsettingsFrameLayout->addWidget(mInitialDistance = new QDoubleSpinBox(),2,1);

        mInitialDistance->setRange(0.1,10.0);
        mInitialDistance->setValue(0.5);
        mInitialDistance->setSingleStep(0.1);

        PMFsettingsFrameLayout->addWidget(new QLabel("Max distance"),3,0);
        PMFsettingsFrameLayout->addWidget(mMaxDistance = new QDoubleSpinBox(),3,1);

        mMaxDistance->setRange(0.1,10.0);
        mMaxDistance->setValue(3.0);
        mMaxDistance->setSingleStep(0.1);

        mPMFsettingsFrame->setLayout(PMFsettingsFrameLayout);
        confFrameLayout->addWidget(mPMFsettingsFrame);
        mPMFsettingsFrame->setVisible(false);


        mPassthroughsettingsFrame = new QFrame();
        QGridLayout *PassthroughsettingsFrameLayout = new QGridLayout();
        PassthroughsettingsFrameLayout->setContentsMargins(0,0,0,0);

        PassthroughsettingsFrameLayout->addWidget(new QLabel("Min Z value"),0,0);
        PassthroughsettingsFrameLayout->addWidget(mMinZValue = new QDoubleSpinBox(),0,1);

        mMinZValue->setRange(-1000,10000);
        mMinZValue->setValue(0);
        mMinZValue->setSingleStep(10);

        mPassthroughsettingsFrame->setLayout(PassthroughsettingsFrameLayout);
        confFrameLayout->addWidget(mPassthroughsettingsFrame);
        mPassthroughsettingsFrame->setVisible(true);

        mPbCompute = new QPushButton("Preview");
        connect(mPbCompute,SIGNAL(clicked()),this,SLOT(on_pbCompute_triggered()));
        confFrameLayout->addWidget(mPbCompute);

        QFrame *acceptCancelFrame = new QFrame();
        QHBoxLayout *acceptCancelFrameLayout = new QHBoxLayout();
        acceptCancelFrameLayout->setContentsMargins(0,0,0,0);
        acceptCancelFrameLayout->addWidget(mPbCancel = new QPushButton("Cancel"));
        acceptCancelFrameLayout->addWidget(mPbAccept = new QPushButton("Accept"));
        connect(mPbCancel,SIGNAL(clicked()),this,SLOT(on_pbCancel_triggered()));
        connect(mPbAccept,SIGNAL(clicked()),this,SLOT(on_pbAccept_triggered()));

        acceptCancelFrame->setLayout(acceptCancelFrameLayout);
        confFrameLayout->addWidget(acceptCancelFrame);

        mPbAccept->setEnabled(false);

        confFrameLayout->addStretch();
        confFrame->setLayout(confFrameLayout);
        mainLayout->addWidget(confFrame);

        mCCVWPreview = new ccViewerWrapper();
        mainLayout->addWidget((QWidget*)mCCVWPreview->getGLWindow());
        setLayout(mainLayout);

        mProgressDialog = new ProgressDialog(this);
        mConsole = new QTextEdit(this);
        QFont* font = new QFont("Courier");
        font->setPixelSize(10);
        mConsole->setFont(*font);
        mConsole->setReadOnly(true);
        mProgressDialog->setConsole(mConsole);
}

GroundSegmentationDialog::~GroundSegmentationDialog()
{
}

void GroundSegmentationDialog::on_pbCompute_triggered(){
    switch (mCbMethod->currentIndex()) {
    case 0:
        mSegmentationProcess = new PassThroughFilterProcess(mSourceFile,mMinZValue->value());
        connect(mSegmentationProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mSegmentationProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mSegmentationProcess, SIGNAL(finished()),this,SLOT(on_segmentationProcessFinished()));

        connect(mSegmentationProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mSegmentationProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle(mSegmentationProcess->getStartupMessage());
        mProgressDialog->setStatusText(mSegmentationProcess->getStartupMessage());
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mSegmentationProcess->start();
        break;
    case 1:
        mSegmentationProcess = new PMFProcess(mSourceFile,mMaxDistance->value(),mSlope->value(),mInitialDistance->value(),mMaxDistance->value());
        connect(mSegmentationProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mSegmentationProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mSegmentationProcess, SIGNAL(finished()),this,SLOT(on_segmentationProcessFinished()));

        connect(mSegmentationProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mSegmentationProcess,SLOT(stop()));

//        mProgressDialog->clearConsole();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle(mSegmentationProcess->getStartupMessage());
        mProgressDialog->setStatusText(mSegmentationProcess->getStartupMessage());
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mSegmentationProcess->start();
        break;
    }
}
void GroundSegmentationDialog::on_segmentationProcessFinished(){

    mProgressDialog->setRange(0,1);
    mProgressDialog->setValue(1);
    mProgressDialog->setStatusText(tr("Process finished."));
    mProgressDialog->setFinished(true);
    mProgressDialog->close();
    mTargetFile = QFileInfo(QFileInfo(mSourceFile).absolutePath()+"/roofs.asc").absoluteFilePath();
    if (!QFileInfo(mTargetFile).exists()) {

        return;
    }
    mCCVWPreview->addToDB(QStringList() << mTargetFile);
    mCCVWPreview->activatePointDisplay();

    mPbAccept->setEnabled(true);

}
void GroundSegmentationDialog::on_CbMethodSelectionChanged(int index){
    switch (index) {
    case 0:
        mPMFsettingsFrame->setVisible(false);
        mPassthroughsettingsFrame->setVisible(true);
        break;
    case 1:
        mPassthroughsettingsFrame->setVisible(false);
        mPMFsettingsFrame->setVisible(true);
        break;
    }
}

void GroundSegmentationDialog::on_pbAccept_triggered(){
    this->accept();
}
void GroundSegmentationDialog::on_pbCancel_triggered(){
    this->close();
}
QString GroundSegmentationDialog::getNewSourceFile(){
    return mTargetFile;
}
