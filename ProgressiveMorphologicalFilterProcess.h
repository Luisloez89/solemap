#ifndef PROGRESSIVEMORPHOLOGICALFILTERPROCESS_H
#define PROGRESSIVEMORPHOLOGICALFILTERPROCESS_H

#include "ProcessManager/ProcessConcurrent.h"
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

class ProgressiveMorphologicalFilterProcess : public ProcessConcurrent
{
public:
    ProgressiveMorphologicalFilterProcess(QString sourceFile,int maxWindowSize, float slope, float initialDistance, float maxDistance);
    ~ProgressiveMorphologicalFilterProcess();

    virtual void run();
private:
    QString mSourceFile;
    int mMaxWindowSize;
    float mSlope,mInitialDistance, mMaxDistance;
    void writeCloudToXYZRGBI_ASCII(QString outputPath, pcl::PointCloud<pcl::PointXYZI>::Ptr &cloud);
};

#endif // PROGRESSIVEMORPHOLOGICALFILTERPROCESS_H
