#include "PassThroughFilterProcess.h"
#include <QDir>
#include <QFileInfo>
PassThroughFilterProcess::PassThroughFilterProcess(QString sourceFile,float minZValue):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/PassThroughFilterSolEMapX64.exe")
{
    //        setWorkingDir(QFileInfo(mCommandPath).absolutePath());
            setWorkingDir(QFileInfo(sourceFile).absolutePath());
            setStartupMessage("Launching passthrough filter...");

            QFileInfo inputFileInfo(sourceFile);

            QStringList inputs;

            inputs << inputFileInfo.absoluteFilePath()<< QString::number(minZValue);

            addIntputs(inputs);
}

PassThroughFilterProcess::~PassThroughFilterProcess()
{

}
