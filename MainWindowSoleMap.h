#ifndef MAINWINDOWSOLEMAP_H
#define MAINWINDOWSOLEMAP_H

#define MAINWINDOWSOLEMAP_PROGRAM_NAME         "SolEMap"
#define MAINWINDOWSOLEMAP_PROGRAM_TAG          "SolEMap"
#define MAINWINDOWSOLEMAP_VERSION              "1.0"
#define MAINWINDOWSOLEMAP_BUILD_NUMBER         "1"
#define MAINWINDOWSOLEMAP_RELEASE_TYPE         "(Release)"

#include <QMainWindow>
#include "ccviewerwrapper.h"
#include "ccCommon.h"
#include "ccGLWindow.h"
#include <ccPointCloud.h>
#include <cc2DLabel.h>
#include <cc2DViewportLabel.h>
#include <QTreeWidget>
#include <QDockWidget>
#include "ProgressDialog.h"
#include "SP_ExtractionProcess.h"
#include <QPushButton>
#include <QDoubleSpinBox>
#include "GroundSegmentationDialog.h"
#include <QLineEdit>
#include <QGroupBox>
namespace Ui {
class MainWindowSoleMap;
}

class MainWindowSoleMap : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowSoleMap(QWidget *parent = 0);
    ~MainWindowSoleMap();

private slots:
    void on_actionExit_triggered();

    void on_action_Load_data_triggered();

private:
    QGroupBox *mResultsLegend;
    QFrame *settingsFrame;
    bool haveWeatherData,haveSourceData;
    Ui::MainWindowSoleMap *ui;
    ccViewerWrapper *mCCVWDataSet, *mCCVWResults;
    QTabWidget *mainTabWidget;
    QTreeWidget *mResultsTreeWidget;
    QDockWidget *mDockResults, *mDockSettings;
    QString mSourceFile;
    ProgressDialog *mProgressDialog;
    QTextEdit *mConsole;
    SP_ExtractionProcess *mExtractionProcess;
    bool removeDir(QString dirName);
    bool copyRecursively(const QString &srcFilePath,
                         const QString &tgtFilePath);

    QPushButton *mPbCompute,*mPbWeatherDataSearch;

    QDoubleSpinBox *mSpDownsamplingDist,*mSpDistanceToRoofClustering, *mSpRansacDistanceToPlaneThreshold,*mSpLatitude,*mSpLongitude,*mspEucliideanClusterToleranceToRemoveOutliers,*mSpObstaclePerimeter;
    QSpinBox *mSpRemainingPointsToContinueSegmentation, *mSpMinPointsToAcceptRansacPlane;

    GroundSegmentationDialog *mGrSegDialog;
    void loadResultsOnResultsTree(QStringList PanelAreaFiles);
private slots:
    void on_extractionProcessFinished();
    void manageProccesStdOutput(QString data);
    void manageProccesErrorOutput(QString data);
    void onError(int code, QString cause);
    void compute();
    void on_actionRemove_Ground_triggered();
    void on_actionAbout_triggered();
    void imageHovered();
    void imageLeft();
    void showWeatherStationWeb();
    void on_searchWeatherData_triggered();
};

#endif // MAINWINDOWSOLEMAP_H
