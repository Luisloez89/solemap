#include "SP_ExtractionProcess.h"
#include <QDir>
SP_ExtractionProcess::SP_ExtractionProcess(QString inputFile,float lat, float lon, float downsamplingDist, float distanceToRoofClustering, int remainingPointsToContinueSeg, float ransacDistanceToPlaneThreshold, int minInliersToAccpetPlane, float euclideanClusterToleranceToRemoveOutliers, float obstaclePerimeter):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/RoofSegmentation.exe")
{
//    QString workingDir = QDir::currentPath()+"/Results";
//    if (QDir(workingDir).exists()) {
//        removeDir(QDir(workingDir).absolutePath());
//    }
//    QDir().mkdir(QDir(workingDir).absolutePath());

//    setWorkingDir(QFileInfo(mCommandPath).absolutePath());
    setWorkingDir(QDir::currentPath()+"/ExternalBinaries");
    setStartupMessage("Extracting locations...");

    if (QDir(QFileInfo(mCommandPath).absolutePath()+"/Results").exists()) {
        removeDir(QDir(QFileInfo(mCommandPath).absolutePath()+"/Results").absolutePath());
    }
//    if (QDir(QFileInfo(mCommandPath).absolutePath()+"/weather_data").exists()) {
//        removeDir(QDir(QFileInfo(mCommandPath).absolutePath()+"/weather_data").absolutePath());
//    }

    QFileInfo inputFileInfo(inputFile);

    QStringList inputs;

//    inputs << inputFileInfo.absoluteFilePath()<< "40.66" << "-4.72" << "0.1" <<"1.0"<< "0.1" << "0.1" << "2000" << "0.25" << "0.5";
    inputs << inputFileInfo.absoluteFilePath()<< QString::number(lat) << QString::number(lon) << QString::number(downsamplingDist) <<QString::number(distanceToRoofClustering) << QString::number(remainingPointsToContinueSeg/100) << QString::number(ransacDistanceToPlaneThreshold) << QString::number(minInliersToAccpetPlane) << QString::number(euclideanClusterToleranceToRemoveOutliers) << QString::number(obstaclePerimeter);

    addIntputs(inputs);
}

SP_ExtractionProcess::~SP_ExtractionProcess()
{

}


bool SP_ExtractionProcess::removeDir(QString dirName)
{
    bool result = true;
    QDir dir(dirName);
    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }
            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }
    return result;
}
