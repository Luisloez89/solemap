#include "MainWindowSoleMap.h"
#include "ui_MainWindowSoleMap.h"

#include "ccHObject.h"
#include "ccPointCloud.h"
#include "ccGLWindow.h"
#include "qCC_db_dll.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QGroupBox>
#include <QLabel>
#include <QTextStream>
#include "AboutSolEMap.h"
#include "ClickableImage.h"
#include <QDesktopServices>
#include <QUrl>
#include <QDesktopWidget>
MainWindowSoleMap::MainWindowSoleMap(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowSoleMap)
{
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->setupUi(this);
    setWindowTitle("SolEMap");
    setWindowIcon(QIcon(":/img/SOLEMAP.png"));
    mainTabWidget = new QTabWidget();
    mainTabWidget->setStyleSheet("background-color:transparent");
    mCCVWDataSet = new ccViewerWrapper();
    mainTabWidget->addTab((QWidget*)mCCVWDataSet->getGLWindow(),tr("Source data"));
    mCCVWResults = new ccViewerWrapper();

    QFrame *resultsFrame = new QFrame();
    resultsFrame->setStyleSheet("background-color:transparent");
    QVBoxLayout *resultsFrameLayout = new QVBoxLayout();
    resultsFrameLayout->setContentsMargins(0,0,0,0);
    resultsFrameLayout->addWidget((QWidget*)mCCVWResults->getGLWindow());

    //Legend
    mResultsLegend = new QGroupBox("Results legend");
    mResultsLegend->setStyleSheet("background-color:#f0f0f0");
    mResultsLegend->setMaximumHeight(50);
    QHBoxLayout *resultsLegendLayout = new QHBoxLayout();
    resultsLegendLayout->setContentsMargins(0,0,0,0);

    QFrame *greenFrame = new QFrame();
    QHBoxLayout *greenFrameLaytout = new QHBoxLayout();
    greenFrameLaytout->addStretch();
    greenFrameLaytout->addWidget(new QLabel("<strong>Suitable surface</strong>"));
    greenFrameLaytout->addStretch();
    greenFrame->setLayout(greenFrameLaytout);
    greenFrame->setStyleSheet("background-color: green");
    QFrame *yellowFrame = new QFrame();
    QHBoxLayout *yellowFrameLaytout = new QHBoxLayout();
    yellowFrameLaytout->addStretch();
    yellowFrameLaytout->addWidget(new QLabel("<strong>Supports necessary surface</strong>"));
    yellowFrameLaytout->addStretch();
    yellowFrame->setStyleSheet("background-color: yellow");
    yellowFrame->setLayout(yellowFrameLaytout);
    QFrame *redFrame = new QFrame();
    QHBoxLayout *redFrameLaytout = new QHBoxLayout();
    redFrame->setStyleSheet("background-color: red");
    redFrameLaytout->addStretch();
    redFrameLaytout->addWidget(new QLabel("<strong>Not suitable surface</strong>"));
    redFrameLaytout->addStretch();
    redFrame->setLayout(redFrameLaytout);
    QFrame *whiteFrame = new QFrame();
    QHBoxLayout *whiteFrameLaytout = new QHBoxLayout();
    whiteFrame->setStyleSheet("background-color: white");
    whiteFrameLaytout->addStretch();
    whiteFrameLaytout->addWidget(new QLabel("<strong>Solar panel areas</strong>"));
    whiteFrameLaytout->addStretch();
    whiteFrame->setLayout(whiteFrameLaytout);
    QFrame *blackFrame = new QFrame();
    QHBoxLayout *blackFrameLaytout = new QHBoxLayout();
    blackFrame->setStyleSheet("background-color: black;color: white");
    blackFrameLaytout->addStretch();
    blackFrameLaytout->addWidget(new QLabel("<strong>Obstacles</strong>"));
    blackFrameLaytout->addStretch();
    blackFrame->setLayout(blackFrameLaytout);
    QFrame *greyFrame = new QFrame();
    QHBoxLayout *greyFrameLaytout = new QHBoxLayout();
    greyFrame->setStyleSheet("background-color: grey");
    greyFrameLaytout->addStretch();
    greyFrameLaytout->addWidget(new QLabel("<strong>Obstacle perimeters</strong>"));
    greyFrameLaytout->addStretch();
    greyFrame->setLayout(greyFrameLaytout);


    resultsLegendLayout->addWidget(greenFrame);
    resultsLegendLayout->addWidget(yellowFrame);
    resultsLegendLayout->addWidget(redFrame);
    resultsLegendLayout->addWidget(whiteFrame);
    resultsLegendLayout->addWidget(blackFrame);
    resultsLegendLayout->addWidget(greyFrame);

    mResultsLegend->setLayout(resultsLegendLayout);
    resultsFrameLayout->addWidget(mResultsLegend);
    resultsFrame->setLayout(resultsFrameLayout);
            //

    mainTabWidget->addTab(resultsFrame,tr("Results"));

//    mainTabWidget->addTab((QWidget*)mCCVWResults->getGLWindow(),tr("Results"));

    mainTabWidget->setTabEnabled(1, false);

    setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    setCentralWidget(mainTabWidget);

    mDockResults = new QDockWidget(tr("Results"),this);
    mDockResults->setFeatures ( QDockWidget::NoDockWidgetFeatures );
    addDockWidget(Qt::LeftDockWidgetArea, mDockResults);
    mDockSettings = new QDockWidget(tr("Advanced settings"),this);
    mDockSettings->setFeatures ( QDockWidget::NoDockWidgetFeatures );
    addDockWidget(Qt::BottomDockWidgetArea, mDockSettings);


    QFrame *settingsFrame = new QFrame();
    QHBoxLayout *settingsFrameLayout = new QHBoxLayout();

    QGroupBox *cloudSettings = new QGroupBox(tr("General settings"));
    QGridLayout *cloudSettingsLayout = new QGridLayout();
    cloudSettingsLayout->addWidget(new QLabel("Latitude"),0,0);
    cloudSettingsLayout->addWidget(mSpLatitude = new QDoubleSpinBox(),0,1);

    mSpLatitude->setRange(-90.00,90.00);
    mSpLatitude->setValue(0.00);
    mSpLatitude->setSingleStep(0.1);

    cloudSettingsLayout->addWidget(new QLabel("Longitude"),1,0);
    cloudSettingsLayout->addWidget(mSpLongitude = new QDoubleSpinBox(),1,1);

    mSpLongitude->setRange(-180.00,180.00);
    mSpLongitude->setValue(0.00);
    mSpLongitude->setSingleStep(0.1);


    cloudSettingsLayout->addWidget(new QLabel("Downsampling distance"),2,0);
    cloudSettingsLayout->addWidget(mSpDownsamplingDist = new QDoubleSpinBox(),2,1);

    mSpDownsamplingDist->setRange(0,1);
    mSpDownsamplingDist->setValue(0.1);
    mSpDownsamplingDist->setSingleStep(0.1);

    cloudSettingsLayout->addWidget(new QLabel("Min distance to roof clustering"),3,0);
    cloudSettingsLayout->addWidget(mSpDistanceToRoofClustering = new QDoubleSpinBox(),3,1);

    mSpDistanceToRoofClustering->setRange(0.5,5.0);
    mSpDistanceToRoofClustering->setValue(1.0);
    mSpDistanceToRoofClustering->setSingleStep(0.1);

    cloudSettingsLayout->addWidget(new QLabel("Historical weather data (epw)"),4,0);

    QFrame *inputNearestWeatherStationFrame = new QFrame();
    QHBoxLayout *inputNearestWeatherStationFrameLayout = new QHBoxLayout();
    inputNearestWeatherStationFrameLayout->setContentsMargins(0,0,0,0);
    inputNearestWeatherStationFrameLayout->addWidget(mPbWeatherDataSearch = new QPushButton("Search"));
    connect(mPbWeatherDataSearch,SIGNAL(clicked()),this,SLOT(on_searchWeatherData_triggered()));

    ClickableImage *NearesStationInfo = new ClickableImage();
    NearesStationInfo->setToolTip("Weather station code info");
    QPixmap  NearesStationInfoPixmap(":/img/info.png");
    NearesStationInfo->setPixmap(NearesStationInfoPixmap.scaled(20,20,Qt::KeepAspectRatio));
    connect(NearesStationInfo,SIGNAL(leftButtonPressed(ClickableImage*)),this,SLOT(showWeatherStationWeb()));
    connect(NearesStationInfo,SIGNAL(mouseEntered(ClickableImage*)),this,SLOT(imageHovered()));
    connect(NearesStationInfo,SIGNAL(mouseLeft(ClickableImage*)),this,SLOT(imageLeft()));
    inputNearestWeatherStationFrameLayout->addWidget(NearesStationInfo);


    inputNearestWeatherStationFrame->setLayout(inputNearestWeatherStationFrameLayout);
    cloudSettingsLayout->addWidget(inputNearestWeatherStationFrame,4,1);


    cloudSettings->setLayout(cloudSettingsLayout);

    QGroupBox *RANSACSettings = new QGroupBox(tr("Plane extraction settings"));
    QGridLayout *RANSACSettingsLayout = new QGridLayout();

    RANSACSettingsLayout->addWidget(new QLabel("Min points to continue plane extraction (%)"),0,0);
    RANSACSettingsLayout->addWidget(mSpRemainingPointsToContinueSegmentation = new QSpinBox(),0,1);

    mSpRemainingPointsToContinueSegmentation->setRange(10,30);
    mSpRemainingPointsToContinueSegmentation->setValue(10);
    mSpRemainingPointsToContinueSegmentation->setSingleStep(1);

    RANSACSettingsLayout->addWidget(new QLabel("Distance to plane threshold"),1,0);
    RANSACSettingsLayout->addWidget(mSpRansacDistanceToPlaneThreshold = new QDoubleSpinBox(),1,1);

    mSpRansacDistanceToPlaneThreshold->setRange(0.05,0.25);
    mSpRansacDistanceToPlaneThreshold->setValue(0.1);
    mSpRansacDistanceToPlaneThreshold->setSingleStep(0.01);

    RANSACSettingsLayout->addWidget(new QLabel("Min plane inliers"),2,0);
    RANSACSettingsLayout->addWidget(mSpMinPointsToAcceptRansacPlane = new QSpinBox(),2,1);

    mSpMinPointsToAcceptRansacPlane->setRange(20,10000);
    mSpMinPointsToAcceptRansacPlane->setValue(2000);
    mSpMinPointsToAcceptRansacPlane->setSingleStep(10);

    RANSACSettingsLayout->addWidget(new QLabel("Min distance to remove plane outliers"),3,0);
    RANSACSettingsLayout->addWidget(mspEucliideanClusterToleranceToRemoveOutliers = new QDoubleSpinBox(),3,1);

    mspEucliideanClusterToleranceToRemoveOutliers->setRange(0.1,2);
    mspEucliideanClusterToleranceToRemoveOutliers->setValue(0.5);
    mspEucliideanClusterToleranceToRemoveOutliers->setSingleStep(0.1);

    RANSACSettings->setLayout(RANSACSettingsLayout);

    QFrame *obstaclesAndBtComputeFrame = new QFrame();
    QVBoxLayout *obstaclesAndBtComputeFrameLayout = new QVBoxLayout();
    obstaclesAndBtComputeFrameLayout->setContentsMargins(0,0,0,0);
    QGroupBox *obstaclesSettings = new QGroupBox(tr("Obstacles settings"));
    QGridLayout *obstaclesSettingsLayout = new QGridLayout();

    obstaclesSettingsLayout->addWidget(new QLabel("Perimeter arround obstacles"),0,0);
    obstaclesSettingsLayout->addWidget(mSpObstaclePerimeter = new QDoubleSpinBox(),0,1);
    mSpObstaclePerimeter->setRange(0.1,1.0);
    mSpObstaclePerimeter->setValue(0.5);
    mSpObstaclePerimeter->setSingleStep(0.1);


    obstaclesSettings->setLayout(obstaclesSettingsLayout);
    obstaclesAndBtComputeFrameLayout->addWidget(obstaclesSettings);

    mPbCompute = new QPushButton("Compute");
    mPbCompute->setEnabled(false);
    connect(mPbCompute,SIGNAL(clicked()),this,SLOT(compute()));
    obstaclesAndBtComputeFrameLayout->addWidget(mPbCompute);
    obstaclesAndBtComputeFrame->setLayout(obstaclesAndBtComputeFrameLayout);
    settingsFrameLayout->addWidget(cloudSettings,1);
    settingsFrameLayout->addWidget(RANSACSettings,1);
    settingsFrameLayout->addWidget(obstaclesAndBtComputeFrame,1);

    settingsFrameLayout->addStretch();

    QLabel *SolEMaplogo = new QLabel();

    SolEMaplogo->setPixmap(QPixmap(":/img/SOLEMAP.png").scaledToHeight(QApplication::desktop()->screenGeometry().height()/6,Qt::SmoothTransformation));
    settingsFrameLayout->addWidget(SolEMaplogo);


    settingsFrame->setLayout(settingsFrameLayout);

    mDockSettings->setWidget(settingsFrame);

    //Setup Project tree:
    mResultsTreeWidget = new QTreeWidget();
    mResultsTreeWidget->setHeaderHidden(true);
    mResultsTreeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    mResultsTreeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    mDockResults->setWidget(mResultsTreeWidget);

    mProgressDialog = new ProgressDialog(this);
    mConsole = new QTextEdit(this);
    QFont* font = new QFont("Courier");
    font->setPixelSize(10);
    mConsole->setFont(*font);
    mConsole->setReadOnly(true);
    mProgressDialog->setConsole(mConsole);

    ui->actionRemove_Ground->setEnabled(false);

    showMaximized();

    haveWeatherData=false;
    haveSourceData=false;
}

MainWindowSoleMap::~MainWindowSoleMap()
{
    delete ui;
}

void MainWindowSoleMap::on_actionExit_triggered()
{
    this->close();
}

void MainWindowSoleMap::on_action_Load_data_triggered()
{
    QString filters("ASCII XYZT file (*.asc)");
    QString defaultFilter ="ASCII XYZT file (*.asc)";

    mSourceFile = QFileDialog::getOpenFileName(this, tr("Open File"), "C://",filters,&defaultFilter);
    if (!mSourceFile.isEmpty()) {
        mCCVWDataSet->addToDB(QStringList() << mSourceFile);
        mCCVWDataSet->activatePointDisplay();
        haveSourceData=true;
        ui->actionRemove_Ground->setEnabled(true);
        mResultsTreeWidget->clear();
        if (haveSourceData && haveWeatherData) {
            mPbCompute->setEnabled(true);
        }
    }
}

void MainWindowSoleMap::on_extractionProcessFinished(){


    if (!mExtractionProcess->isStopped()) {
        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setStatusText(tr("Process finished."));
        mProgressDialog->setFinished(true);
        //Mover resultados a la hubicación del archivo
        QDir defaultResultsDir(QDir::currentPath()+"/ExternalBinaries/Results");
        QDir ResultsDir(QFileInfo(mSourceFile).absolutePath()+"/Results");

        bool success= copyRecursively(defaultResultsDir.absolutePath(),ResultsDir.absolutePath());

        //Leer los resultados y mostrarlos en pantalla
        QDir panelAreasDir(ResultsDir.absolutePath()+"/PanelAreas");
        QStringList PanelAreaFiles = panelAreasDir.entryList(QDir::NoDotAndDotDot | QDir::Files);//(QDir::Filter::Files,QDir::SortFlag::NoSort)
        QDir SegmentedCloudsDir(ResultsDir.absolutePath()+"/RoofSegmentationClouds");
        QStringList SegmentedCloudFiles = SegmentedCloudsDir.entryList(QDir::NoDotAndDotDot | QDir::Files);//(QDir::Filter::Files,QDir::SortFlag::NoSort)

        for (int i = 0; i < PanelAreaFiles.count(); ++i) {
            PanelAreaFiles[i]=QFileInfo(panelAreasDir.absolutePath()+"/"+PanelAreaFiles[i]).absoluteFilePath();
        }
        for (int i = 0; i < SegmentedCloudFiles.count(); ++i) {
            SegmentedCloudFiles[i]=QFileInfo(SegmentedCloudsDir.absolutePath()+"/"+SegmentedCloudFiles[i]).absoluteFilePath();
        }
        mCCVWResults->addToDB(QStringList() << PanelAreaFiles << SegmentedCloudFiles);
//        mCCVWResults->activatePointDisplay();

//        mCCVWResults->getGLWindow()->setPickingMode(ccGLWindow::DEFAULT_PICKING);
        mainTabWidget->setTabEnabled(1, true);
        loadResultsOnResultsTree(PanelAreaFiles);
        mainTabWidget->setCurrentIndex(1);
    }
}

void MainWindowSoleMap::manageProccesStdOutput(QString data)
{
    mConsole->append("<font color='black'>" + data + "\n</font>");
}

void MainWindowSoleMap::manageProccesErrorOutput(QString data)
{
    mConsole->append("<font color='#999900'>" + data + "</font>");
}

void MainWindowSoleMap::onError(int code, QString cause)
{

}

bool MainWindowSoleMap::removeDir(QString dirName)
{
    bool result = true;
    QDir dir(dirName);
    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }
            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }
    return result;
}

bool MainWindowSoleMap::copyRecursively(const QString &srcFilePath,
                            const QString &tgtFilePath)
{
    QFileInfo srcFileInfo(srcFilePath);
    if (srcFileInfo.isDir()) {
        QDir targetDir(tgtFilePath);
        targetDir.cdUp();
        if (!targetDir.mkdir(QFileInfo(tgtFilePath).fileName()))
            return false;
        QDir sourceDir(srcFilePath);
        QStringList fileNames = sourceDir.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden | QDir::System);
        foreach (const QString &fileName, fileNames) {
            const QString newSrcFilePath
                    = srcFilePath + QLatin1Char('/') + fileName;
            const QString newTgtFilePath
                    = tgtFilePath + QLatin1Char('/') + fileName;
            if (!copyRecursively(newSrcFilePath, newTgtFilePath))
                return false;
        }
    } else {
        if (!QFile::copy(srcFilePath, tgtFilePath))
            return false;
    }
    return true;
}

void MainWindowSoleMap::compute(){
    QDir resultsPath(QFileInfo(mSourceFile).absolutePath()+"/Results");
    if (resultsPath.exists()) {
        QMessageBox msgBox(this);
        msgBox.setText("Current results will be removed.");
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        int ret = msgBox.exec();
        if (ret!= QMessageBox::Ok) {
            return;
        }
        removeDir(resultsPath.absolutePath());
    }

    mExtractionProcess = new SP_ExtractionProcess(mSourceFile,mSpLatitude->value(),mSpLongitude->value(),mSpDownsamplingDist->value(),mSpDistanceToRoofClustering->value(),mSpRemainingPointsToContinueSegmentation->value(),mSpRansacDistanceToPlaneThreshold->value(),mSpMinPointsToAcceptRansacPlane->value(),mspEucliideanClusterToleranceToRemoveOutliers->value(),mSpObstaclePerimeter->value());
    connect(mExtractionProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
    connect(mExtractionProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
    connect(mExtractionProcess, SIGNAL(finished()),this,SLOT(on_extractionProcessFinished()));

    connect(mExtractionProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
    connect(mProgressDialog,SIGNAL(cancel()),mExtractionProcess,SLOT(stop()));

    mConsole->clear();
    mProgressDialog->setModal(true);
    mProgressDialog->setRange(0,0);
    mProgressDialog->setWindowTitle(mExtractionProcess->getStartupMessage());
    mProgressDialog->setStatusText(mExtractionProcess->getStartupMessage());
    mProgressDialog->setFinished(false);
    mProgressDialog->show();
    mExtractionProcess->start();
}

void MainWindowSoleMap::on_actionRemove_Ground_triggered()
{
    mGrSegDialog = new GroundSegmentationDialog(this,mSourceFile);
    if (!mGrSegDialog->exec()) {
        return;
    }
    mSourceFile = mGrSegDialog->getNewSourceFile();
    mCCVWDataSet->addToDB(QStringList() << mSourceFile);
    mCCVWResults->addToDB(QStringList());
    mainTabWidget->setCurrentIndex(0);
}

void MainWindowSoleMap::on_actionAbout_triggered()
{
    AboutSolEMap *aboutDialog = new AboutSolEMap(this);
    aboutDialog->exec();
}

void MainWindowSoleMap::loadResultsOnResultsTree(QStringList PanelAreaFiles)
{
    float totalPanelAreas=0, totalSolarIrr=0;
    QTreeWidgetItem *rootItem, *panelCoordinatesItem,*coordItem,*areaItem,*SolarIrrItem;
    foreach (QString panelAreaFilePath, PanelAreaFiles) {
        QFileInfo panelAreaFileInfo(panelAreaFilePath);
        rootItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(panelAreaFileInfo.baseName()));
        //panel vertex coordinates
        panelCoordinatesItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList("Panel vertex coordinates"));

        QFileInfo coordinatesFileInfo(QFileInfo(mSourceFile).absolutePath()+"/Results/Rectangles/"+panelAreaFileInfo.baseName()+".txt");
        QFile coordsFile(coordinatesFileInfo.absoluteFilePath());
        if (coordsFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&coordsFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList lineSplited = line.split(" ");
              coordItem =new QTreeWidgetItem((QTreeWidget*)0, QStringList("("+lineSplited[0]+", "+lineSplited[1]+")"));
              panelCoordinatesItem->addChild(coordItem);
           }
           coordsFile.close();
        }
        rootItem->addChild(panelCoordinatesItem);
        //Panel area

        QFileInfo areaFileInfo(QFileInfo(mSourceFile).absolutePath()+"/Results/PanelArea_m2/"+panelAreaFileInfo.baseName()+".txt");
        QFile areaFile(areaFileInfo.absoluteFilePath());
        if (areaFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&areaFile);

           QString line = in.readLine();
           line = in.readLine();
           totalPanelAreas+=line.toFloat();
           areaItem =new QTreeWidgetItem((QTreeWidget*)0, QStringList("Panel area: "+line+" m"+QChar(0xB2)));
           rootItem->addChild(areaItem);
           areaFile.close();
        }

        //Yearly solar irradiation

        QFileInfo yearlySolarIrrFileInfo(QFileInfo(mSourceFile).absolutePath()+"/Results/SolarIrradiationPanel/"+panelAreaFileInfo.baseName()+".txt");
        QFile yearlySolarIrrFile(yearlySolarIrrFileInfo.absoluteFilePath());
        if (yearlySolarIrrFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&yearlySolarIrrFile);

           QString line = in.readLine();
           totalSolarIrr+=line.toFloat();
           SolarIrrItem =new QTreeWidgetItem((QTreeWidget*)0, QStringList("Panel yearly solar irradiation: "+line+" kWh/a"));
           rootItem->addChild(SolarIrrItem);
           yearlySolarIrrFile.close();
        }

        QList<QTreeWidgetItem*> items = mResultsTreeWidget->findItems(tr(""),Qt::MatchStartsWith);
        mResultsTreeWidget->insertTopLevelItem(items.count(),rootItem);
    }

    //Final results
    rootItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Global results")));
    QFont GlobalResultsFont("" , 9 , QFont::Bold );
    rootItem->setFont(0,GlobalResultsFont);
    //Panel area
    areaItem =new QTreeWidgetItem((QTreeWidget*)0, QStringList("Total panels area: "+QString::number(totalPanelAreas)+" m"+QChar(0xB2)));
    rootItem->addChild(areaItem);

    //Yearly solar irradiation
    SolarIrrItem =new QTreeWidgetItem((QTreeWidget*)0, QStringList("Total yearly solar irradiation: "+QString::number(totalSolarIrr)+" kWh/a"));
    rootItem->addChild(SolarIrrItem);

    mResultsTreeWidget->insertTopLevelItem(0,rootItem);

}

void MainWindowSoleMap::imageHovered(){
    this->setCursor(Qt::PointingHandCursor);
}

void MainWindowSoleMap::imageLeft(){
    this->setCursor(Qt::ArrowCursor);
}

void MainWindowSoleMap::showWeatherStationWeb(){
    QDesktopServices::openUrl(QUrl("https://www.energyplus.net/weather"));
}

void MainWindowSoleMap::on_searchWeatherData_triggered(){
    QString filters("Weather data file (*.epw)");
    QString defaultFilter ="Weather data file (*.epw)";

    QString weatherFile = QFileDialog::getOpenFileName(this, tr("Open weather data file"), "C://",filters,&defaultFilter);
    if (!weatherFile.isEmpty()) {
        QDir weatherData(QDir::currentPath()+"/ExternalBinaries/weather_data");
        if (weatherData.exists()) {
            removeDir(weatherData.absolutePath());
        }
        QDir().mkdir(weatherData.absolutePath());

        QFileInfo weatherFileInfo(weatherFile);
        QFile::copy(weatherFileInfo.absoluteFilePath(), QFileInfo(weatherData.absolutePath()+"/"+weatherFileInfo.completeBaseName()+"."+weatherFileInfo.suffix()).absoluteFilePath());
        haveWeatherData=true;

        if (haveSourceData && haveWeatherData) {
            mPbCompute->setEnabled(true);
        }
    }
}
