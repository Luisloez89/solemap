#ifndef GROUNDSEGMENTATIONDIALOG_H
#define GROUNDSEGMENTATIONDIALOG_H

#include <QDialog>
#include "ccviewerwrapper.h"
#include "ccCommon.h"
#include "ccGLWindow.h"
#include <ccPointCloud.h>
#include <cc2DLabel.h>
#include <cc2DViewportLabel.h>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include "ProcessManager/ExternalProcess.h"
#include "ProgressDialog.h"

class GroundSegmentationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GroundSegmentationDialog(QWidget *parent = 0,QString sourceFile ="");
    ~GroundSegmentationDialog();
    QString getNewSourceFile();
private:
    ccViewerWrapper *mCCVWPreview;
    QComboBox *mCbMethod;
    QSpinBox *mMaxWindowSize;
    QDoubleSpinBox *mSlope,*mInitialDistance, *mMaxDistance ,*mMinZValue;
    QPushButton *mPbCompute,*mPbAccept,*mPbCancel;
    QString mSourceFile,mTargetFile;
    ExternalProcess *mSegmentationProcess;
    ProgressDialog *mProgressDialog;
    QFrame *mPassthroughsettingsFrame,*mPMFsettingsFrame;
    QTextEdit *mConsole;
private slots:
    void on_pbCompute_triggered();
    void on_segmentationProcessFinished();
    void on_CbMethodSelectionChanged(int index);
    void on_pbAccept_triggered();
    void on_pbCancel_triggered();
};

#endif // GROUNDSEGMENTATIONDIALOG_H
