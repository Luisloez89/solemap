/********************************************************************************
** Form generated from reading UI file 'AboutSolEMap.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTSOLEMAP_H
#define UI_ABOUTSOLEMAP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_AboutSolEMap
{
public:

    void setupUi(QDialog *AboutSolEMap)
    {
        if (AboutSolEMap->objectName().isEmpty())
            AboutSolEMap->setObjectName(QString::fromUtf8("AboutSolEMap"));
        AboutSolEMap->resize(400, 300);

        retranslateUi(AboutSolEMap);

        QMetaObject::connectSlotsByName(AboutSolEMap);
    } // setupUi

    void retranslateUi(QDialog *AboutSolEMap)
    {
        AboutSolEMap->setWindowTitle(QApplication::translate("AboutSolEMap", "Dialog", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AboutSolEMap: public Ui_AboutSolEMap {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTSOLEMAP_H
