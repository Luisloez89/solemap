#include "AboutSolEMap.h"
#include <QVBoxLayout>
//#include <QStyleOptionTab>
#include "DependencyInfoWidget.h"
#include "MainWindowSoleMap.h"
AboutSolEMap::AboutSolEMap(QWidget *parent) :
    QDialog(parent)
{
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowTitle(tr("About SolEMap"));
    setStyleSheet("background-color:white;");
    setMinimumSize(700,700);
    setMaximumSize(700,700);

    //About SolEMap
    QVBoxLayout *mainFrameLayout = new QVBoxLayout();
    QFrame *logoFrame = new QFrame();
    QHBoxLayout *logoFrameLayout = new QHBoxLayout();

    QLabel *lbLogoSolEMap = new QLabel();
    QPixmap pix(":/img/SOLEMAP.png");
    lbLogoSolEMap->setPixmap(pix);
    lbLogoSolEMap->setStyleSheet("background:transparent;");
    logoFrameLayout->addStretch();
    logoFrameLayout->addWidget(lbLogoSolEMap);
    logoFrameLayout->addStretch();

    logoFrame->setLayout(logoFrameLayout);
    mainFrameLayout->addWidget(logoFrame);

    QFrame *infoSolEMapFrame = new QFrame();
    QVBoxLayout *infoSolEMapFrameLayout = new QVBoxLayout();
    QTextEdit *textArea= new QTextEdit();
    textArea->setFrameStyle(QFrame::NoFrame);
    textArea->setReadOnly(true);
    textArea->setText("<h2>SolEMap v " + QString( MAINWINDOWSOLEMAP_VERSION) +" build number "+
                                                                      QString(MAINWINDOWSOLEMAP_BUILD_NUMBER)  +
                                                                      QString(MAINWINDOWSOLEMAP_RELEASE_TYPE) + "</h2>"
                          "<font size=\"4\">An Open Source tool for the authomatic evaluation of 3D points clouds with thermographic information for smart cities. "
                          "SolEMap has been conceived as a tool to automate the evaluation of large areas to locate the optimal places to install solar panels. The evaluation of the roofs geometry is complemented with "
                          "the thermographic information and historical weather data allowing to identify posible obstacles and to estimate the solar irradiation incident on each surface.<br><strong>License:</strong> GPL v3</font>"
                          );

    infoSolEMapFrameLayout->addWidget(textArea);

    QFrame *authorsDependenciesFrame = new QFrame();
    QGridLayout *authorsDependenciesFrameLayout = new QGridLayout();
    authorsDependenciesFrameLayout->setContentsMargins(0,0,0,0);

    QLabel *authorsLabel= new QLabel("<h2>Authors</h2>");
    authorsDependenciesFrameLayout->addWidget(authorsLabel,0,0);

    QFrame *authorsFrame = new QFrame();
    QGridLayout *authorsFrameLayout = new QGridLayout();
    authorsFrameLayout->setContentsMargins(0,0,0,0);

    QLabel *lbLogoUSAL = new QLabel();
    QPixmap pixUsal(":/img/Tidop_en.png");
    lbLogoUSAL->setPixmap(pixUsal);
    lbLogoUSAL->setStyleSheet("background:transparent;");


    authorsFrameLayout->addWidget(lbLogoUSAL,0,1);

    QLabel *usalAuthors = new QLabel("<strong>  Luis L&#0243;pez Fern&#0225;ndez</strong><br/>luisloez89@usal.es<br/>"
                            "<strong>  Susana Lag&#252;ela</strong><br/>sulaguela@usal.es<br/>"
                                     "<strong>  Diego Gonz&#0225;lez Aguilera</strong><br/>daguilera@usal.es<br/>"
                                     "<strong>  Pablo Rodr&#0237;guez Gonz&#0225;lvez</strong><br/>pablorgsf@usal.es<br/>"
                                     "<strong>David Hern&#0225;ndez L&#0243;pez</strong><br/>david.hernandez@uclm.es<br/>"
                                     "<strong>Diego Guerrero Sevilla</strong><br/>dguerrero@uclm.es<br/>");




    authorsFrameLayout->addWidget(usalAuthors,1,1, Qt::AlignTop);

    authorsFrame->setLayout(authorsFrameLayout);
    authorsDependenciesFrameLayout->addWidget(authorsFrame,1,0);

    authorsDependenciesFrame->setLayout(authorsDependenciesFrameLayout);
    infoSolEMapFrameLayout->addWidget(authorsDependenciesFrame);
    infoSolEMapFrame->setLayout(infoSolEMapFrameLayout);

    mainFrameLayout->addWidget(infoSolEMapFrame);

    //Dependencies

    QFrame *infoToolsFrame = new QFrame();
    QVBoxLayout *infoToolsFrameLayout = new QVBoxLayout();

    authorsDependenciesFrameLayout->addWidget(new QLabel("<h2>Dependencies</h2>"),0,1);

    QTabWidget *tabWidgetTools = new QTabWidget;
//    tabWidgetTools->setMaximumWidth(350);
//    tabWidgetTools->setTabPosition(QTabWidget::South);
    tabWidgetTools->addTab(new DependencyInfoWidget("GPL v2","CloudCompare is a 3D point cloud (and triangular mesh) processing software. "
                                                                       "It has been originally designed to perform comparison between two 3D points clouds (such as the ones "
                                                                                            "obtained with a laser scanner) or between a point cloud and a triangular mesh. "
                                                                                            "It relies on a specific octree structure that enables great performances1 in this "
                                                                                            "particular function.","http://www.danielgm.net/cc/"), "CloudCompare");
    tabWidgetTools->addTab(new DependencyInfoWidget("BSD","The Point Cloud Library (or PCL) is a large scale, open project for 2D/3D image and point cloud processing. The PCL framework contains numerous state-of-the art algorithms including filtering, feature estimation, surface reconstruction, registration, model fitting and segmentation. These algorithms can be used, for example, to filter outliers from noisy data, stitch 3D point clouds together, segment relevant parts of a scene, extract keypoints and compute descriptors to recognize objects in the world based on their geometric appearance, and create surfaces from point clouds and visualize them -- to name a few.","https://www.http://pointclouds.org/"), "PCL");



    tabWidgetTools->addTab(new DependencyInfoWidget("GPL v3","CGAL is a software project that provides easy access to efficient and reliable geometric algorithms in the form of a C++ library. CGAL is used in various areas needing geometric computation, such as geographic information systems, computer aided design, molecular biology, medical imaging, computer graphics, and robotics.","http://www.cgal.org/index.html"), "CGAL");

    tabWidgetTools->addTab(new DependencyInfoWidget("MIT","A simple experimental utility to estimate the solar potential of building roof surface(s) from a 3D city model stored in CityGML.","https://github.com/tudelft3d/Solar3Dcity"), "Solar3DCity");

//    tabWidgetTools->addTab(new DependencyInfoWidget("Free for research and non-commercial use","Photogrammetric Surface Reconstruction from Imagery. SURE is a software solution for multi-view stereo, which enables the derivation of dense point clouds from a given set of images and its orientations. Up to one 3D point per pixel is extracted, which enables a high resolution of details. It is based on libtsgm, which implements the core functionality for image rectification, dense matching and multi-view triangulation, and provides a C/C++ API.","http://www.ifp.uni-stuttgart.de/publications/software/sure/index.en.html"), "SURE");


    infoToolsFrame->setLayout(infoToolsFrameLayout);
    authorsDependenciesFrameLayout->addWidget(tabWidgetTools,1,1);


    setLayout(mainFrameLayout);
}

AboutSolEMap::~AboutSolEMap()
{
}

