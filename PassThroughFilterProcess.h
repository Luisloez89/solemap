#ifndef PASSTHROUGHFILTERPROCESS_H
#define PASSTHROUGHFILTERPROCESS_H
#include "ProcessManager/ExternalProcess.h"


class PassThroughFilterProcess: public ExternalProcess
{
public:
    PassThroughFilterProcess(QString sourceFile,float minZValue);
    ~PassThroughFilterProcess();

};

#endif // PASSTHROUGHFILTERPROCESS_H
