![SOLEMAP.png](https://bitbucket.org/repo/9qk8xA/images/3200041779-SOLEMAP.png)

SolEMap v1.0

Homepage: http://www.luislf.es


License: GPL v2

Introduction

SolEMap is a 3D point cloud with thermographic information processing tool to locate the optimal location for the installation of solar panels over roof surfaces. The tool allows efficiently process large surfaces.

Reference:

López-Fernández, L.; Lagüela, S.; Picón, I.; González-Aguilera, D.	Large Scale Automatic Analysis and Classification of Roof Surfaces for the Installation of Solar Panels Using a Multi-Sensor Aerial Platform. Remote Sens. 2015, 7, 11226-11248.

http://www.mdpi.com/2072-4292/7/9/11226

Contact:

Luis López Fernández
Luisloez89@usal.es