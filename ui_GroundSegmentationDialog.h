/********************************************************************************
** Form generated from reading UI file 'GroundSegmentationDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GROUNDSEGMENTATIONDIALOG_H
#define UI_GROUNDSEGMENTATIONDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_GroundSegmentationDialog
{
public:

    void setupUi(QDialog *GroundSegmentationDialog)
    {
        if (GroundSegmentationDialog->objectName().isEmpty())
            GroundSegmentationDialog->setObjectName(QString::fromUtf8("GroundSegmentationDialog"));
        GroundSegmentationDialog->resize(400, 300);

        retranslateUi(GroundSegmentationDialog);

        QMetaObject::connectSlotsByName(GroundSegmentationDialog);
    } // setupUi

    void retranslateUi(QDialog *GroundSegmentationDialog)
    {
        GroundSegmentationDialog->setWindowTitle(QApplication::translate("GroundSegmentationDialog", "Dialog", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GroundSegmentationDialog: public Ui_GroundSegmentationDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GROUNDSEGMENTATIONDIALOG_H
